import { config } from "./config"

const cloud = tt.createCloud({
  envID: config.envID,
  serviceID: config.serviceID
});

/**
 * 服务调用
 * @param {*} options 
 */
export const request = (options) => {
  // console.info(options.url,"  ",options.data)
  options.method && (options.method = options.method.toUpperCase())
  return new Promise((resolve, reject) => {
    // 根据config设定调用方式调用对应服务
    if ("cloud" === config.serviceMethod) {
      callContainerMethod(options, resolve, reject)
    }else if ("back" === config.serviceMethod) {
      requestMethod(options, resolve, reject)
    }
  })
}

/**
 * 文件上传
 * @param {*} options 
 */
export const upload = (options) => {
  return new Promise((resolve, reject) => {
    if("cloud" === config.fileMethod) {
      uploadCallContainerMethod(options, resolve, reject)
    }else if ("back" === config.fileMethod) {
      uploadRequestMethod(options, resolve, reject)
    }
  })
}

/**
 * 调用抖音云上传方法
 * @param {*} options 
 * @param {*} resolve 
 * @param {*} reject 
 */
function uploadCallContainerMethod(options, resolve, reject) {
  tt.login({
    success: (res) => {
      console.info("---- tt.login调用成功，准备调用上传")
      cloudUpLoadFile(options, resolve, reject)
    },
    fail: (res) => {
      
    },
  });

}
  /**
   * 文件存储器-上传
   * @param {*} options 
   * @param {*} resolve 
   * @param {*} reject 
   */
  function cloudUpLoadFile(options, resolve, reject) {
    console.info("cloudPath-"+options.path, "filePath-"+options.data)
    const uploadTask = cloud.uploadFile({
      cloudPath: options.path,
      filePath: options.data,
      timeout: 80000,
      success: (res) => {
        console.info("Cloud.uploadFile success", res)
        resolve(res)
      },
      fail: (err) => {
        console.log("Cloud.uploadFile fail", err);
        reject(err)
      },
      complete: (res) => {
        console.log("Cloud.uploadFile complete", res);
      },
    })
    console.info("uploadTask",uploadTask)
  }


/**
 * 调用服务器上传方法
 * @param {*} options 
 * @param {*} resolve 
 * @param {*} reject 
 */
function uploadRequestMethod(options, resolve, reject) {
  
    tt.uploadFile({
      url: options.url,
      filePath: options.data.path,
      name: "file",
      header: {"Content-Type": "multipart/form-data"},
      success: (res) => {
        resolve(res.data)
      },
      fail: (res) => {
        reject(res)
      },
    });
  
}

/**
 * 使用request的方法调用后台服务
 */
function requestMethod(options, resolve, reject) {
    tt.request({
      url:  options.url,
      method: options.method || "POST",
      data: options.data || {},
      header: {
        'content-type': 'application/json',
      },
      success: (res) => {
        resolve(res.data)
      },
      fail: (err) => {
        reject(err)
      },
    });
}

/**
 * 使用callContainer调用抖音云服务
 */
function callContainerMethod(options, resolve, reject) {
  cloud.callContainer({
    // path: `/api/comInfo/searchComPostInfo`,
    path: options.url,
    init: {
      method: 'POST',
      header: {'content-type': 'application/json',},
      body: options.data,
      timeout: 60000, //ms
    },
    success: ({ statusCode, header, data }) => {
      // console.info("data:",data)
      resolve(JSON.parse(data))
    },
    fail: (err) => {
            reject(err)
          },
    // fail: console.warn,
    // complete: console.log,
  })
}