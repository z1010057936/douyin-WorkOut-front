
import { config }  from './config'

var back = config.backIp;


export const devUrl = {
    sysConfig: {
        searchSysConfig: back +"/sysConfig/searchSysConfig", // 系统配置查询
        sysConfigManager: back + "/sysConfig/sysConfigManager", // 系统配置维护
        getCusUnionid: back + "/sysConfig/getCusUnionid", // 通过抖音服务器的code2Session获取uinionid
        getDict: back + "/sysConfig/getDict", //获取字典码
        writePoint: back + "/sysConfig/writePoint", // 埋点数据写入
        writePointList: back + "/sysConfig/writePointList", // 埋点数据查询
        dyDecrypt: back + "/sysConfig/dyDecrypt", // 抖音敏感信息解密处理
    },
    comInfo: {
        searchComPostInfo: back + "/comInfo/searchComPostInfo", // 企业职业列表查询
        searchComPostDetail: back + "/comInfo/searchComPostDetail", // 企业职业详情查询
        customJoinLogManager: back + "/comInfo/customJoinLogManager", // 用户报名职位管理
        searchCustomJoinLog: back + "/comInfo/searchCustomJoinLog", //用户报名职位查询
        searchComDetailAndBanners: back + "/comInfo/searchComDetailAndBanners", //查询企业信息及对应banner图
        searchComDistributionAddress: back + "/comInfo/searchComDistributionAddress", // 查询企业分布地址
    },
    resume: {
        searchCustomResume: back + "/resume/searchCustomResume", // 用户简历信息查询
        customResumeManage: back + "/resume/customResumeManage", // 用户简历信息维护
        customLogin: back + "/resume/customLogin", //用户登录
        customLoginInfoCheck: back + "/resume/customLoginInfoCheck", //用户登录前置检查
        customResumeInit: back + "/resume/customResumeInit", // 简历信息初始化
    },
    manager: {
        searchPostDetailAndJoins: back + "/manager/searchPostDetailAndJoins", // 查询有效职位列表及留资者信息
        searchComDetail: back + "/manager/searchComDetail", // 企业信息列表查询
        comDetailManager: back + "/manager/comDetailManager", // 企业信息管理
        searchJoinsUserList: back + "/manager/searchJoinsUserList", // 根据postId查询报名者列表
        editJoinsUserInfo: back + "/manager/editJoinsUserInfo", //修改报名信息
        searchPostDetail: back + "/manager/searchPostDetail", //职位信息列表查询
        postManager: back + "/manager/postManager", //职位信息管理
        searchPostContext: back + "/manager/searchPostContext", //职位信息详细内容查询
        postContextManager: back + "/manager/postContextManager" ,//职位信息详细内容查询
        imgUploader: back + "/manager/imgUploader", // 图片上传接口
        searchContactHistory: back + "/manager/searchContactHistory", // 查询联络历史
        contactManager: back + "/manager/contactManager", // 联络历史管理
        postImgManager: back + "/manager/postImgManager", // 职位图片管理
        comResultAndSortManager: back + "/manager/comResultAndSortManager", //企业信息发布状态及排序变更
    }
}
