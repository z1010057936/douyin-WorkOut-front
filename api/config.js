export const config = {

    appSecret: '80acb4c27d7c0dae1029b06ff6c5b77c75f335af',
    appID: 'tt741a02a7eb286bd901',
    fileMethod: 'cloud', // 文件上传地址-至抖音云
    fileBasePath: 'https://tt741a02a7eb286bd901-env-3bwsrdssr7.tos-cn-beijing.volces.com/', 
    fastJoinComponentsId: "f6071f2a726bfa7e9b0e5ca16c8b2469", // 快速投递，线索组件ID

    serviceMethod: 'cloud', // 服务调用地址- 至抖音云
    backIp: '/api', // 后端地址配置

    // serviceMethod: 'back', // 服务调用地址- 至服务器
    // backIp: "http://localhost:8000/api", // 后端地址配置

    
    envID: 'env-3BwSrdSsr7', // 小程序ID
    serviceID: '1jbvdy7npabzx',  // 生产服务ID
    // serviceID: '1jbvdxvri4hlv', // 开发服务id
    
    
}