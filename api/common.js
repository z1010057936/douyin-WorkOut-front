import { request,upload } from '../api/request';
// import { data } from '../api/data';
import { devUrl } from '../api/urlDetail';
 

//  export function getShopList() {
//     return request({
//       url: `http://localhost:3000/`, //开发者后端url使用者可自行替换
//     });
//   }

//   export function getOrderList() {
//     return request({
//       url: `http://localhost:3000/`, //开发者后端url使用者可自行替换
//     });
//   }
//以下为模拟
 export function getShopList() {
    // return request({
    //       url: `http://localhost:8080/jobHunter/hello`, //开发者后端url使用者可自行替换
    //     });
    return data
  }

  /**
   * 查询系统配置
   * @param {*} data 
   */
  export function searchSysConfig(data) {
    return request({
      url: devUrl.sysConfig.searchSysConfig, 
      data: data
    })
  }

  /**
   * 系统配置维护
   * @param {*} data 
   */
  export function sysConfigManager(data) {
    return request({
      url: devUrl.sysConfig.sysConfigManager, 
      data: data
    })
  }
  

  /**
   * 企业职业列表查询
   * @param {*} data 
   */
  export function getComPostInfo(data) {
    // return data
    return request({
          url: devUrl.comInfo.searchComPostInfo, 
          data: data
        });
        
  }

  /**
   * 企业职业详情查询
   * @param {*} data 
   */
  export function getComPostDetail(data) {
    // return data
    return request({
          url: devUrl.comInfo.searchComPostDetail,
          data: data
        });
        
  }

  /**
   * 查询用户简历信息
   * @param {*} data 
   */
  export function getCustomResume(data) {
    // return data.customResume
    return request({
      url: devUrl.resume.searchCustomResume,
      data: data
    });
  }

  /**
   * 用户简历信息管理
   * @param {*} data 
   */
  export function customResumeManage(data) {
    return request({
      url: devUrl.resume.customResumeManage,
      data: data
    })
  }

  /**
   * 用户登录
   * @param {*} data 
   */
  export function customLogin(data) {
    return request({
      url: devUrl.resume.customLogin,
      data: data
    });
  }

  /**
   * 用户登录前置检查
   * @param {*} data 
   */
  export function customLoginInfoCheck(data) {
    return request({
      url: devUrl.resume.customLoginInfoCheck,
      data: data
    });
  }

  /**
   * 用户报名职位查询
   * @param {*} data 
   */
  export function searchCustomJoinLog(data) {
    return request({
      url: devUrl.comInfo.searchCustomJoinLog,
      data: data
    });
  }


  /**
   * 用户报名职位管理
   * @param {*} data 
   */
  export function customJoinLogManager(data) {
    return request({
      url: devUrl.comInfo.customJoinLogManager,
      data: data
    });
  }

  /**
   * 通过抖音服务器的code2Session获取uinionid
   * @param {*} data 
   */
  export function getCusUnionid(data) {
    return request({
      url: devUrl.sysConfig.getCusUnionid,
      data: data
    })
  }

  /**
   * 查询有效职位列表及留资者信息
   * @param {*} data 
   */
  export function searchPostDetailAndJoins(data) {
    return request({
      url: devUrl.manager.searchPostDetailAndJoins,
      data: data
    })
  }

  /**
   * 企业信息管理
   * @param {*} data 
   */
  export function comDetailManager(data) {
    return request({
      url: devUrl.manager.comDetailManager,
      data: data
    })
  }

  /**
   * 企业信息列表查询
   * @param {*} data 
   */
  export function searchComDetail(data) {
    return request({
      url: devUrl.manager.searchComDetail,
      data: data
    })
  }

  
  /**
   * 查询企业信息及对应banner图
   * @param {*} data 
   */
   export function searchComDetailAndBanners(data) {
    return request({
      url: devUrl.comInfo.searchComDetailAndBanners,
      data: data
    })
  }

  /**
   * 查询报名者列表
   * @param {*} data 
   */
  export function searchJoinsUserList(data) {
    return request({
      url: devUrl.manager.searchJoinsUserList,
      data: data
    })
  }

  /**
   * 获取字典码
   * @param {*} data 
   */
   export function getDict(data) {
    return request({
      url: devUrl.sysConfig.getDict,
      data: data
    })
  }

  /**
   * 修改报名信息
   * @param {*} data 
   */
  export function editJoinsUserInfo(data) {
    return request({
      url: devUrl.manager.editJoinsUserInfo,
      data: data
    })
  }

  
  /**
   * 职位信息列表查询
   * @param {*} data 
   */
   export function searchPostDetail(data) {
    return request({
      url: devUrl.manager.searchPostDetail,
      data: data
    })
  }

  /**
   * 职位信息管理
   * @param {*} data 
   */
   export function postManager(data) {
    return request({
      url: devUrl.manager.postManager,
      data: data
    })
  }

  /**
   * 职位信息详细内容查询
   * @param {*} data 
   */
   export function searchPostContext(data) {
    return request({
      url: devUrl.manager.searchPostContext,
      data: data
    })
  }

  /**
   * 职位信息详细内容维护
   * @param {*} data 
   */
   export function postContextManager(data) {
    return request({
      url: devUrl.manager.postContextManager,
      data: data
    })
  }

  /**
   * 查询联络历史
   * @param {*} data 
   */
   export function searchContactHistory(data) {
    return request({
      url: devUrl.manager.searchContactHistory,
      data: data
    })
  }

  /**
   * 联络历史管理
   * @param {*} data 
   */
   export function contactManager(data) {
    return request({
      url: devUrl.manager.contactManager,
      data: data
    })
  }

  /**
   * 图片上传接口
   * @param {*} data 
   */
  export function imgUploader(data) {
    return upload({
      url: devUrl.manager.imgUploader,
      data: data
    })
  }

  /**
   * 企业宣传图片上传
   * @param {*} data 
   */
   export function comImgUploader(path, data) {
    return upload({
      path: path,
      data: data
    })
  }

  /**
   * 职位图片管理
   * @param {*} data 
   */
 export function postImgManager(data) {
  return request({
    url: devUrl.manager.postImgManager,
    data: data
  })
}

/**
   * 企业信息发布状态及排序变更
   * @param {*} data 
   */
 export function comResultAndSortManager(data) {
  return request({
    url: devUrl.manager.comResultAndSortManager,
    data: data
  })
}

/**
   * 简历信息初始化
   * @param {*} data 
   */
 export function customResumeInit(data) {
  return request({
    url: devUrl.resume.customResumeInit,
    data: data
  })
}

/**
   * 埋点数据写入
   * @param {*} data 
   */
 export function writePoint(data) {
  return request({
    url: devUrl.sysConfig.writePoint,
    data: data
  })
}

/**
   * 埋点数据查询
   * @param {*} data 
   */
 export function writePointList(data) {
  return request({
    url: devUrl.sysConfig.writePointList,
    data: data
  })
}

/**
 * 抖音敏感信息解密处理
 * @param {*} data.appid 小程序 ID
 * @param {*} data.secret 小程序的 APP Secret，可以在开发者后台获取
 * @param {*} data.encryptedData 包括敏感数据在内的完整用户信息的加密数据
 * @param {*} data.iv 加密算法的初始向量
 * @param {*} data.code 临时登录凭证, 有效期 5 分钟。开发者可以通过在服务端调用 登录凭证校验接口 换取 openid 和 session_key 等信息。
 * @param {*} data.anonymousCode 用于标识当前设备, 无论登录与否都会返回, 有效期 5 分钟。
 */
export function dyDecrypt(data) {
  return request({
    url: devUrl.sysConfig.dyDecrypt,
    data: data
  })
}

/**
 * 查询企业分布地址
 * @param {*} data 
 */
export function searchComDistributionAddress(data) {
  return request({
    url: devUrl.comInfo.searchComDistributionAddress,
    data:data
  })
}
