// /Users/bytedance/Desktop/codelabs-miniprogram-template/miniprogram-templates/templates/microapp/javascript/group-buy-industry/group-buy-industry/components/index/index.js
var publicFunction = require('../../utils/publicFunction');
import {
  customJoinLogManager
} from "../../api/common";
Component({
  data: {
    haveDatas: false,
    loginLoadding: false,
    nowData: null
  },
  properties: {
    postList: {
      type: Array,
    },
    fromPage: String,
    isLogin: Boolean
  },

  // attached(options) {
    // const isLogin = tt.getStorageSync("isLogin");
    // this.setData({
    //   isLogin
    // })
    // console.info("this.properties.fromPage",this.properties.fromPage)
  // },

  methods: {
    /**
     * 一个用于阻止父级组件事件冒泡的空方法，顺便塞个当前点击的列数据
     */
    noFunction(e) {
      this.setData({
        nowData: e
      })
    },

    /**
     * 点击卡片，跳转
     */
    toProductDetail(e) {

      const item = JSON.stringify(e.currentTarget.dataset.item);
      tt.navigateTo({
        url: `/pages/product-detail/product-detail?fromPage=${this.properties.fromPage}&postList=${item}`,
        success: (res) => {

        },
        fail: (res) => {
          console.log(res);
        },
      });
    },
    //调用用户拨打电话功能
    tapMakePhoneCall(e) {
      publicFunction.tapMakePhoneCall(e)
    },
    //立即报名，可触发接口
    joinPost(e) {
      publicFunction.joinPost(e)
    },

    //表单校验
    formRule(e) {
      var result = true;
      const keys = Object.keys(e);
      keys.forEach(function (item, key) {
        if (e[item] === "" || e[item] === null || e[item].length === 0) {
          result = false;
        }
      })
      return result;
    },

    // 触发报名接口
    async customJoinLogManagerInterFace(data) {
      //这里调用后台报名接口
      const res = await customJoinLogManager(data)
      if (0 === res.err_no) {
        tt.showToast({
          title: '报名成功'
        });
      } else {
        tt.showToast({
          title: res.err_msg,
          icon: 'fail'
        });
      }
    },
    /**
     * 一键登录
     * @param {*} e 
     */
    async douyinLogin(e) {
      console.log("---- 点击登录", this.data.openCounts)
      this.setData({
        loginLoadding: true
      })
      const res = await publicFunction.douyingetPhoneNum(e)
      if(null !== res) {
        await publicFunction.resumeInit(res, this)
        tt.showTabBar({animation: false})
        await publicFunction.joinPost(this.data.nowData)
        this.setData({
          loginLoadding: false
        })
      }else {
        tt.showTabBar({animation: false})
        this.setData({
          loginLoadding: false
        })
      }
    }

  }
})