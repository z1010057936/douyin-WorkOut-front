// /Users/bytedance/Desktop/codelabs-miniprogram-template/miniprogram-templates/templates/microapp/javascript/group-buy-industry/group-buy-industry/components/index/index.js
var publicFunction = require('../../utils/publicFunction');
import { config } from  "../../api/config"
import {
  customJoinLogManager
} from "../../api/common";
Component({
  data: {
    isLogin: false,
    haveDatas: false,
    fileBasePath: config.fileBasePath, // 文件基础地址，从config.js中取
  },
  properties: {
    comList: {
      type: Array,
    },
    fromPage: String
  },

  // attached(options) {
  //   console.info("postList",this.properties.postList)
  //   if(this.properties.postList.length > 0) {
  //     this.setData({
  //       haveDatas: true
  //     })
  //   }
  // },
  attached(options) {
    // let fileBasePath = config.fileBasePath
    // this.setData({
    //   fileBasePath
    // })
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom(e) {
    console.info(e)
  },

  methods: {

    toProductDetail(e) {

      const item = JSON.stringify(e.currentTarget.dataset.item);
      tt.navigateTo({
        url: `/pages/com-detail/com-detail?fromPage=${this.properties.fromPage}&comList=${item}`,
        success: (res) => {

        },
        fail: (res) => {
          console.log(res);
        },
      });
    },
    //调用用户拨打电话功能
    tapMakePhoneCall(e) {
      publicFunction.tapMakePhoneCall(e)
    },
    //立即报名，可触发接口
    joinPost(e) {
      publicFunction.joinPost(e)
    },

    //表单校验
    formRule(e) {
      var result = true;
      const keys = Object.keys(e);
      keys.forEach(function (item, key) {
        if (e[item] === "" || e[item] === null || e[item].length === 0) {
          result = false;
        }
      })
      return result;
    },

    // 触发报名接口
    async customJoinLogManagerInterFace(data) {
      //这里调用后台报名接口
      const res = await customJoinLogManager(data)
      if (0 === res.err_no) {
        tt.showToast({
          title: '报名成功'
        });
      } else {
        tt.showToast({
          title: res.err_msg,
          icon: 'fail'
        });
      }
    },

  }
})