const app = getApp();

import {
  searchComDetailAndBanners,
} from "../../api/common";
var publicFunction = require('../../utils/publicFunction');
Component({
  data: {
    // leftNavHeight: null,
    comInfoList: [], // 企业列表
    comPageParams: {
      page: 1,
      limit: 10
    },
    comMaxPages: 1,
    currentIndex: 0, // 点击态下标
  },
  properties: {
    addressList: {
      type: Array
    },
    navHeight: {
      type: Number
    }
  },
  attached(options) {
    // 动态设置左边分类栏高度
    // this.setData({
    //   leftNavHeight:this.properties.windowHeight/2 - 130,
    // })
    // console.info("systemInfo",systemInfo)
    // 查询第一个地址的企业信息
    if(this.properties.addressList.length > 0) {
      this.resetPageParams(this.properties.addressList[0].city)
      this.thisGetComInfo()
    }
    
  },
  methods: {
    /**
     * 切换城市
     */
    checkCity(e) {
      const { city,index } = e.currentTarget.dataset
      // console.info(city,index)
      this.resetPageParams(city)
      tt.showLoading({title: '请稍后...',});
      this.thisGetComInfo()
      this.setData({
        currentIndex: index
      })
    },

    /**
     * 触发企业底部事件
     */
    comInfoIsLower(e) {
      if(this.data.comMaxPages === this.data.comPageParams.page) {
        tt.showToast({
          title: "没有更多数据了",
          icon: "none"
        });
        return
      }else {
        this.data.comPageParams.page ++
        tt.showLoading({title: "请稍后...",});
        this.thisGetComInfo()
      }
    },

    /**
     * 重置查询条件
     * @param {*} city 城市
     */
    resetPageParams(city) {
      this.setData({
        comPageParams: {
          page: 1,
          limit: 10,
          city
        },
        comInfoList: []
      })
    },

    /**
     * 查询企业信息
     */
    async thisGetComInfo() {
      const comRes = await searchComDetailAndBanners(this.data.comPageParams);
      let rawComList = this.data.comInfoList
      if (0 == comRes.err_no) {
        let comList = comRes.data.records;
        // 处理图片字符串，并逐个获取本地路径
        for (let index = 0; index < comList.length; index++) {
          const item = comList[index];
          let bannerImgUrl = item.bannerImgUrl
          if (null === bannerImgUrl || '' === bannerImgUrl || undefined === bannerImgUrl) {
            continue
          } else {
            // 分割图片地址字符串
            let bannerImgUrlArr = bannerImgUrl.split(',')

            // 从缓存获取图片地址，如果不存在则从云端获取
            for (let index = 0; index < bannerImgUrlArr.length; index++) {
              const item = bannerImgUrlArr[index];
              let localPath = await publicFunction.imgGetLocalStorage(item)
              // console.info("resultPath", localPath)
              bannerImgUrlArr[index] = localPath
            }

            // 将处理过的图片数组塞给原数据
            item.bannerImgUrl = bannerImgUrlArr;

          }
        }
        // push进列表数组里
        rawComList.push(...comList)
        tt.hideLoading({});
        this.setData({
          comInfoList: rawComList,
          comMaxPages: comRes.data.pages
        })
      }else {
        tt.hideLoading({});
        tt.showToast({
          title: comRes.err_msg,
          icon: "fail"
        });
      }
    }

    }

})