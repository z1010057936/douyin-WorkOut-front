var publicFunction = require('../../utils/publicFunction');
import { config }  from '../../api/config'

Component({
  data: {
    isTiptrue: false,
    openCounts: 0,
    loginLoadding: false,
    fastJoinComponentsId: null
  },
  properties: {

  },
  attached(options) {
    this.setData({
      fastJoinComponentsId: config.fastJoinComponentsId
    })
    let openCounts = tt.getStorageSync("openCounts")
    let isLogin = tt.getStorageSync("isLogin");
    // console.log("---- 登录弹窗触发", (openCounts == undefined || openCounts == '' || 2 > Number(openCounts)) && false == isLogin)
    if ((openCounts == undefined || openCounts == '' || 2 > Number(openCounts)) && false == isLogin) { //根据缓存周期决定是否显示新手引导
      this.setData({
        isTiptrue: true,
      })
      tt.hideTabBar({animation: false})
    } else {
      this.setData({
        isTiptrue: false,
      })
      tt.showTabBar({animation: false})
    }
    this.setData({
      openCounts
    })
  },
  methods: {
    /**
     * 抖音登录接口
     */
    async douyinLogin(e) {
      console.log("---- 登录弹窗点击登录", this.data.openCounts)
      this.setData({
        loginLoadding: true
      })
      const res = await publicFunction.douyingetPhoneNum(e)
      if(null !== res) {
        publicFunction.resumeInit(res, this)
        tt.showTabBar({animation: false})
        this.setData({
          loginLoadding: false
        })
        this.triggerEvent("loginResult", {"isLogin": true})
      }else {
        tt.showTabBar({animation: false})
        tt.showToast({
          title: "登录失败，请稍后重试",
          icon: "fail"
        });
        this.setData({
          loginLoadding: false
        })
        this.triggerEvent("loginResult", {"isLogin": false})
      }
    },
    /**
     * 点击取消后
     */
    closeThis(e) {
      tt.showTabBar({animation: false})
      console.log("---- 登录弹窗点击取消", this.data.openCounts)
      tt.setStorageSync("openCounts", Number(this.data.openCounts) + 1)
      this.setData({
        isTiptrue: false
      })
    },
    /**
     * 表单提交方法
     */
    formSubmit(e) {

    }
  }
})