var publicFunction = require('../../../utils/publicFunction');
var validate = require("../../../utils/validata");
import {
  writePointList
} from "../../../api/common";
const app = getApp();
Page({
  data: {
    iPhoneBottomHeight:null,
    addOrEidtMask: false, // 是否开启蒙层
    maskInfo:{}, // 蒙层表单数据
    maskBtnLoading: false, // 蒙层表单按钮加载动画
    list: [], // 查询回来的数据信息
    pointTypeList:[],
    maskKeyDisabledFlag: false, // 蒙层中的“键”可否输入开关
    // 字典值
    dict: {
      newUser: {0: "否", 1: "是"}
    },
    pointArr:[],
    pointIndex: 0,
    //表头
    tabelHeads:[],
  },
  onLoad: function (options) {
    const { systemInfo } = app.globalData;
    this.setData({
      iPhoneBottomHeight:systemInfo.screenHeight - systemInfo.safeArea.bottom,
    })
    // 初始化表格
    const { pointType } = tt.getStorageSync("dict")
    // console.info(pointType)

    this.setData({
      "dict[pointType]": pointType,
      pointTypeList: Object.keys(pointType)
    })

    // 优先查询登录埋点信息
    this.getInfo()
  },

  /**
   * 获取埋点数据
   */
  async getInfo() {
    tt.showLoading({title: "请稍后...",});
    let pointCode = this.data.pointTypeList[this.data.pointIndex];
    const res = await writePointList({"pointCode": pointCode});
    if(!res) {
      tt.showToast({title:"系统配置查询失败",icon:"fail"})
    }else{
      if(0 === res.err_no) {
        this.tableHeadFormatter(pointCode)
        this.tableDataFormatter(res.data)
        tt.hideLoading({});
      }else {
        tt.hideLoading({});
        tt.showToast({title:res.err_msg,icon:"fail"})
      }
    }
  },

  /**
   * 表头格式化
   * @param {*} pointType 
   */
  tableHeadFormatter(pointType) {
    let tabelHeads = [];
    let pointTypes = this.data.dict.pointType[pointType]
    // console.info("pointTypes",pointTypes)
    Object.keys(pointTypes).forEach(key => {
      let widthSizi = 250;
      if("createTime" === key) {
        widthSizi = 300
      }
      tabelHeads.push({"prop": key, "label": pointTypes[key], width: widthSizi})
    });
    // console.info("tabelHeads",tabelHeads)
    this.setData({
      tabelHeads,
    })
  },

  /**
   * 表数据格式化
   * @param {*} data 
   */
  tableDataFormatter(data) {
    let tableDatas = []
    data.forEach(temp => {
      tableDatas.push(JSON.parse(temp.datas))
    });
    // console.info("tableDatas",tableDatas)
    this.setData({
      list: tableDatas
    })
  },

  /**
   * 点击数据行
   * @param {*} e 
   */
  onRowClick(e) {
    return
    this.setData({
      maskInfo: {},
      maskKeyDisabledFlag: true
    })
    let thisData = e.detail.currentTarget.dataset.it
    this.setData({
      addOrEidtMask: true,
      maskInfo: thisData
    })
    this.data.maskInfo.optFlag = 1
  },

  /**
   * 埋点码选择变更
   * @param {*} e 
   */
   chosseChange(e) {
    this.setData({
      pointIndex: e.detail.value
    })
  },

  /**
   * 点击搜索
   */
  searchPost() {
    this.getInfo()
  }

})