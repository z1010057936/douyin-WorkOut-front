var publicFunction = require('../../../utils/publicFunction');
import {
  // searchComDetail,
  // searchPostDetailAndJoins,
  searchPostDetail,
  postManager
} from "../../../api/common";
Page({
  data: {
    postList: [],
    account: "Admin",
    comArr: [],
    comIndex: 0,
    statusIndex: 0,
    dict: {},
    statusArr: [
      {name: "-----", value: null},
      {name: "草稿", value: 0},
      {name: "已发布", value: 1},
    ],
    tittleInput:"",
    deleteArr:{}
  },
  onLoad: function (options) {
    // this.searchComDetail()
    let comArrList = tt.getStorageSync("comArr");
    comArrList.unshift({"comId":"","name":"-----"})
    
    const dictList = tt.getStorageSync("dict");
    const { comArr } = dictList;
    this.setData({
      comArr: comArrList,
      dict: {
        comArr
      }
    })
    this.searchPost()
    // this.searchPostDetailAndJoins()
  },

  /**
   * 企业名称选择变更
   * @param {*} e 
   */
  comChosseChange(e) {
    this.setData({
      comIndex: e.detail.value
    })
  },
  /**
   * 发布状态选择变更
   * @param {*} e 
   */
   statusChosseChange(e) {
    this.setData({
      statusIndex: e.detail.value
    })
  },

  /**
   * 职位名称输入赋值
   * @param {*} e 
   */
  tittleNameInput(e) {
    this.setData({
      tittleInput: e.detail.value
    })
  },

  /**
   * 搜索有效职位列表
   */
   async searchPost() {
    this.setData({
      postList: {}
    })
    // console.info(this.data.comArr, this.data.tittleLikeName)
    let comId =  this.data.comArr.length == 0?"":this.data.comArr[this.data.comIndex].comId;
    let status = this.data.statusArr.length == 0?"":this.data.statusArr[this.data.statusIndex].value;
    let tittleLikeName = this.data.tittleInput;
    
    const res = await searchPostDetail({"comId":comId, "status":status, "tittleLikeName":tittleLikeName})
    // console.info("res",res)
    if(0 === res.err_no) {
      this.setData({
        postList: res.data
      })
    }else {
      tt.showToast({title:res.err_msg, icon:"fail"})
    }
    tt.hideLoading({});
  },
  // /**
  //  * 企业信息列表查询
  //  */
  //  async searchComDetail() {
  //   const res = await searchComDetail({});
  //   if(0 === res.err_no) {
  //     let conList = res.data;
  //     let comArr = [];
  //     comArr.push({"comId":"","name":"-----"})
  //     for (let index = 0; index < conList.length; index++) {
  //       const element = conList[index];
  //       comArr.push({"comId":element.comId,"name":element.name})
  //     }
  //     this.setData({
  //       comArr
  //     })
  //   }else {
  //     tt.showToast({title:res.err_msg, icon:"fail"})
  //   }
  //   tt.hideLoading({});
  // },

  /**
   * 跳转到职位详细
   * @param {*} e 
   */
  toUserList(e) {
      const postInfo = e.currentTarget.dataset.postinfo
      const comArr = this.data.comArr
      const postList = JSON.stringify(postInfo)
      const comList = JSON.stringify(comArr)
      tt.navigateTo({
        url: "/components/managerPage/post-detail/post-work-tabel/post-work-tabel?postList="+postList+"&comList="+comList,
        success: (res) => {

        },
        fail: (res) => {
          console.log(res);
        },
      });
  },
  /**
   * 职位删除
   */
  deletePost() {
    let deleteArr = Object.keys(this.data.deleteArr)
    // console.info("deleteArr",deleteArr.length)
    if(deleteArr.length === 0) {
      tt.showToast({
        title: "未选择任何职位",
        icon:"fail"
      });
      return
    }
    // console.info(deleteArr)
    tt.showModal({
      title:"删除",
      content:"要删除已勾选的内容吗",
      success: (res) => {
        if(res.confirm) {
          this.deletePostImpl(deleteArr)
        }
      },
      fail: (res) => {
        
      },
    });
    
  },
  /**
   * 实际调用删除接口
   * @param {*} deleteArr 
   */
  async deletePostImpl(deleteArr) {
    const res = await postManager({"optFlag":2,"deleteArr":deleteArr});
    if (!res) {
      return;
    }else {
      if(0 === res.err_no) {
        tt.showToast({title: "删除成功"});
        this.searchPost()
      } else {
        tt.showToast({title: res.err_msg,icon:"fail"});
      }
    }
  },
  /**
   * 增加新的职位
   * @param {*} e 
   */
  addNewPost(e) {
      const comArr = this.data.comArr
      const comList = JSON.stringify(comArr)
      tt.navigateTo({
        url: "/components/managerPage/post-detail/post-work-tabel/post-work-tabel?comList="+comList,
        success: (res) => {

        },
        fail: (res) => {
          console.log(res);
        },
      });
  },

  /**
   * 选中此项职位
   * @param {*} e 
   */
  checkThisData(e) {
    const { postid } = e.currentTarget.dataset
    const { value } = e.detail
    if(value) {
      this.data.deleteArr[postid] = postid
    }else {
      delete this.data.deleteArr[postid]
    }
  }

})