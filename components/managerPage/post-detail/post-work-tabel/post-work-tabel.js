var publicFunction = require('../../../../utils/publicFunction');
var validate = require("../../../../utils/validata");

import {
  getComPostDetail,
  postContextManager,
  postManager,
} from "../../../../api/common";
const app = getApp();
Page({

  data: {
    iPhoneBottomHeight:null,
    postList: null,
    posts: null,
    starChecked: false,
    bindSucess:false,
    isLogin:false,
    current:0,
    ishidden: true,
    labelList: null,
    comArr: [], // 公司下拉框数据数组
    comIndex: 0, // 公司下拉框默认选项
    addOrEidtMask: false, // 是否开启蒙层
    maskInfo:{}, // 蒙层表单数据
    maskBtnLoading: false, // 蒙层表单按钮加载动画
    baseBtnLoading: false, // 基本信息提交按钮加载动画
    imgUploadLoading: false, // 图片上传按钮加载动画
    imageList: [], // 图片选择列表
    countIndex: 8,
    count: [1, 2, 3, 4, 5, 6, 7, 8, 9],
    salaryUnitArray: ["/月", "/日"],
    salaryUnitIndex: 0,
    mainLabelIndex: 0,
    mainLabeArr:["长白工","日结工","小时工"],

    // 设置字段校验规则
    initValidate: {
      tittle: {
        required: '请填写标题',
      },
      label: {
        required: '请填写标签',
      },
      mainLabel: {
        required: '请填写主标签',
      },
      comInfo: {
        required: '请选择所属企业',
      },
      postName:{
        required: "请输入职位名称",
        cc: "请输入正确的职位名称"
      }
  },
  },
  async onUnload(options) {
    this.gotoBack()
  },
  async onLoad(options) {
    const { systemInfo } = app.globalData;
    this.setData({
      iPhoneBottomHeight:systemInfo.screenHeight - systemInfo.safeArea.bottom,
    })

    

    const { postList } = options;
    // console.info("postList",postList)
    let { comList } = options;

    // 处理企业选择列表，处理当前所属企业
    let comArr = []
    let comIndex = 0
    if (comList.length > 0) {
      comArr = JSON.parse(comList)
      comArr.shift()
      // console.info("comArr",comArr)
      
    }

    // console.info(comList)
    // 如果postList为空，则表示本次是修改操作，将数据带入
    if(!publicFunction.isEmpty(postList)) {
      let thisPostList = JSON.parse(postList)
      const { label } = thisPostList;
      const { postId } = thisPostList;
      const { mainLabel } = thisPostList;
      const datas = {"postId": postId}
      // 查询职位详细内容
      this.getComPostDetail(datas);
      comArr.forEach((element,index) => {
        // console.info(element,index)
        // console.info(element.comId,index,thisPostList.comId)
        if(element.comId === thisPostList.comId) {
          comIndex = index
        }
      });
      //处理一下期望薪资的格式 xxx-xxx/月
      let { salary } = thisPostList
      // console.info("salary",salary)
      // 检查用户输入的符号是什么
      let salary2CharOfE = salary.indexOf("-")
      let salary2CharOfC = salary.indexOf("—")
      //检查符号位置
      let salary2Char = salary2CharOfE == -1?salary2CharOfC:salary2CharOfE;
      // 检查单位位置
      let salary4Char = salary.indexOf("/")

      // 分割字符串
      let salary1 = salary.substr(0,salary2Char);
      // let salary2 = salary.substr(salary2Char,salary4Char)
      let salary3 = salary.substr(salary2Char+1,salary4Char-salary2Char-1)
      let salary4 = salary.substr(salary4Char,salary.length)
      // console.info(salary1,salary3,salary4)

      thisPostList.salary1 = salary1;
      thisPostList.salary3 = salary3;
      thisPostList.salary4 = "/月"==salary4?0:1;

      // 主标签转换
      let mainLabelIndex = this.data.mainLabeArr.indexOf(mainLabel)
      this.setData({
        postList: thisPostList,
        labelList: label.split(","),
        comIndex,
        mainLabelIndex
      })
    }
    
    this.setData({
      comArr
    })
    
  },

  /**
   * 清空选择的图片列表
   * @param {*} e 
   */
  clearImg(e) {
    this.setData({
      imageList: []
    })
  },

  /**
   * 保存图片列表
   * @param {*} e 
   */
  saveImg(e) {
    console.info(e)
    console.info(this.data.imageList)
  },

  /**
   * 主标签选择变更
   * @param {*} e 
   */
  mainLabelChosseChange(e) {
    this.setData({
      mainLabelIndex: e.detail.value
    })
  },

  /**
   * 企业名称选择变更
   * @param {*} e 
   */
   comChosseChange(e) {
    this.setData({
      comIndex: e.detail.value
    })
  },

  /**
   * 查询职位详细内容
   * @param {*} datas 
   */
  async getComPostDetail(datas) {
    tt.showLoading({
      title: "请稍后...",
      mask: true
    });
    const res = await getComPostDetail(datas)
    if (!res) {
      return;
    }

    // console.info(res)
    const postsContext = res.data.postContextDetail;
    let swiperImg = res.data.postsContextGraphicDetail;
    // 如果介绍图片为空，则塞入一个默认图片
    // if(swiperImg.length == 0) {
    //   swiperImg = [{imageUrl:"../../assets/comBanner/notBanner.png"}]
    // }
    this.setData({
      // shopList: res.companies?.slice(0,3),
      swiperImg,
      posts: postsContext,
    })
    tt.hideLoading({});
  },

  /**
   * 基础信息提交
   * @param {*} e 
   */
  baseInfoSubmit(e) {
    let formInfo = e.detail.value;
    formInfo.optFlag = ""
    if(validate.initFormRule(formInfo, this.data.initValidate) === false) {
      return
    }

    //转换薪资的格式
    if("" === formInfo.salary1 || "" === formInfo.salary3) {
      tt.showModal({
        title: "请输入期望薪资"
      });
      return
    }
    if (Number(formInfo.salary1) > Number(formInfo.salary3)) {
      tt.showModal({
        title: "期望薪资格式不正确",
        content:"期望薪资起始值不能大于结束值",
        showCancel:false
      });
      return
    }
    formInfo.salary = formInfo.salary1 + "-" + formInfo.salary3 + "" + this.data.salaryUnitArray[formInfo.salary4]

    // 校验标签数量及单个的长度
    // console.info(formInfo)
    let { label } = formInfo;
    let labelArr = label.split(",")
    if(label.indexOf("，") !== -1) {
      tt.showModal({title:"错误",content:"请使用英文逗号“,”分隔",icon:"fail",showCancel:false})
      return
    }
    if(labelArr.length >= 6) {
      tt.showModal({title:"错误",content:"标签数量不能大于5个",icon:"fail",showCancel:false})
      return
    }
    for (let index = 0; index < labelArr.length; index++) {
      const tempData = labelArr[index];
      if(tempData.length >= 4) {
        tt.showModal({title:"错误",content:"标签["+tempData+"]长度不能大于3",icon:"fail",showCancel:false})
        return
      }
    }
    // 转换企业信息
    let comInfo = this.data.comArr[formInfo.comInfo]
    formInfo.comId = comInfo.comId

    // 转换是否热门职业
    // console.info("formInfo.comInfo",formInfo.hotFlag)
    let hotFlag = formInfo.hotFlag == true || formInfo.hotFlag == 1?1:0
    formInfo.hotFlag = hotFlag;
    this.setData({
      baseBtnLoading: true
    })
    if(null == this.data.postList) {
      formInfo.optFlag = 0
    }else {
      formInfo.optFlag = 1
    }

    // 转换主标签
    let mainLabel = this.data.mainLabeArr[this.data.mainLabelIndex]
    formInfo.mainLabel = mainLabel
    console.info(formInfo)
    // 表单提交
    this.postManager(formInfo);
  },

  /**
   * 职位基础信息管理
   * @param {*} datas 
   */
  async postManager(datas) {
    const res = await postManager(datas);
    const result = publicFunction.baseImplToast(res, "保存成功");
    
    if(result && datas.optFlag == 0) {
      datas.postId = res.data.postId
      datas.status = 0
    }else{
      datas.status = this.data.postList.status
    }
    
    this.setData({
      baseBtnLoading: false,
      postList:datas
    })
    // console.info(this.data.postList)
  },


  /**
   * 工作内容管理
   * @param {*} datas 
   */
  async postContextManager(datas) {
    const res = await postContextManager(datas)
    publicFunction.baseImplToast(res, "保存成功");
  },

  /**
   * 删除工作内容
   * @param {*} datas 
   */
   async postContextdelete(datas) {
    const res = await postContextManager(datas)
    publicFunction.baseImplToast(res, "删除成功");
  },

  /**
   * 当触发返回时
   */
  gotoBack() {
    let pages = getCurrentPages();
    let beforePage = pages[pages.length -1];
    // console.info("pages",pages)
    // console.info("beforePage",beforePage)
    beforePage.searchPost();
  },

  /**
   * 增加详细内容项
   * @param {*} e 
   */
  addNewContext(e) {
    if(null == this.data.postList) {
      tt.showToast({title:"请先保存基本信息",icon:"fail"})
      return
    }
    this.setData({
      maskInfo: {},
    })
    const postId = e.currentTarget.dataset.postid;
    this.setData({
      maskInfo: {
        "postId":postId,
        "optFlag":0
      },
      addOrEidtMask: true
    })
  },

  /**
   * 删减详细内容项
   * @param {*} e 
   */
  reduceThisContext(e) {
    const contextId = e.currentTarget.dataset.contextid;
    const postId = e.currentTarget.dataset.postid;
    const tittle = e.currentTarget.dataset.tittle;
    // console.info(contextId)
    tt.showModal({
      title:"删除",
      content:"确定要删除【"+tittle+"】吗",
      success: (res) => {
        if (res.confirm) {
          this.postContextdelete({"contextId":contextId,"optFlag":2})
          setTimeout(() => {
            this.getComPostDetail({"postId": postId});
            this.setData({
              addOrEidtMask: false,
              maskBtnLoading: false
            })
          }, 1000);
        }
      },
    });
  },

  /**
   * 修改详细内容项
   * @param {*} e 
   */
  editThisContext(e) {
    const thisinfo = e.currentTarget.dataset.thisinfo;
    // console.info(e)
    this.setData({
      maskInfo: thisinfo,
      addOrEidtMask: true
    })
  },

  /**
   * 蒙层表单按钮区-保存
   * @param {*} e 
   */
  contextSubmit(e) {
    this.setData({
      maskBtnLoading: true
    })
    const thisinfo = e.detail.value;
    const { contextId } = thisinfo;
    const { postId } = thisinfo;
    // console.info(thisinfo, contextId)
    // 如果contextId为空则代表本次为增加操作，否则为修改
    tt.showLoading({
      title: "请稍后...",
      mask: true
    });
    if(publicFunction.isEmpty(contextId)) {
      this.setData({maskInfo: thisinfo})
      
      thisinfo.optFlag = 0
      this.postContextManager(thisinfo)
    }else {
      thisinfo.optFlag = 1
      this.postContextManager(thisinfo)
    }
    setTimeout(() => {
      this.getComPostDetail({"postId": postId});
      this.setData({
        addOrEidtMask: false,
        maskBtnLoading: false,
        maskInfo: {}
      })
    }, 1000);
    tt.hideLoading({})
  },

  /**
   * 蒙层表单按钮区-取消
   * @param {*} e 
   */
  contextCancel(e) {
    this.setData({
      addOrEidtMask: false
    })
  },

  /**
   * 发布或停止发布
   * @param {*} e 
   */
  changeStatus(e) {

    const { changestatus } = e.currentTarget.dataset;
    const { postid } = e.currentTarget.dataset;
    console.info(e.currentTarget.dataset)
    let msg = "发布"
    if(0 == changestatus) {
      msg = "停止发布"
    }

    tt.showModal({
      title:"提示",
      content:"确定要【"+msg+"】吗",
      success: (res) => {
        if (res.confirm) {
          // 表单提交
          this.postManager({"optFlag":3,"postId":postid,"status":Number(changestatus)});
          setTimeout(() => {
            tt.navigateBack({delta:-1});
          }, 1000);
        }
      },
    });
  },

  /**
   * 薪资单位变更
   * @param {*} e 
   */
  salaryUnitChange(e) {
    this.setData({
      salaryUnitIndex: e.detail.value
    });
  },


  
})