import {
  searchJoinsUserList
} from "../../../api/common";
Page({
  data: {
    userList: [],
    // 字典值
    dict: {},
    tabelHeads:[
      {
        prop: 'name',
        width: 140,
        label: '姓名',
      },
      {
        prop: 'sex',
        width: 90,
        label: '性别',
      },
      {
        prop: 'joinCount',
        width: 100,
        label: '报名数'
      },
      {
        prop: 'contactFlag',
        width: 150,
        label: '联系结果',
      },
      {
        prop: 'contactTime',
        width: 240,
        label: '联系时间',
        color: '#55C355'
      }
    ]
  },
  onLoad: function (options) {
    const dictList = tt.getStorageSync("dict");
    this.setData({
      dict: {
        sex: dictList.publicDict.sex,
        contactFlag: dictList.publicDict.contactFlag
      }
    })
    this.searchJoinsUserList(options)
  },

  /**
   * 查询有效职位列表及留资者信息
   */
   async searchJoinsUserList(options) {
    tt.showLoading({title: "请稍后..."});
    const managerInfo = tt.getStorageSync("managerInfo");
    let {adminAccount} = managerInfo
    if("admin" == adminAccount) {
      adminAccount = "";
    } 
    const res = await searchJoinsUserList({"account":adminAccount})
    if(0 === res.err_no) {
      this.setData({
        userList: res.data
      })
    }else {
      tt.showToast({title:res.err_msg, icon:"fail"})
    }
    tt.hideLoading({});
  },
   /** 
   * 点击表格一行
   */
    onRowClick: function(e) {
      let datas = e.detail.currentTarget.dataset.it
      // console.log('e: ', datas)
      tt.navigateTo({
        url: `/components/managerPage/join-detail/contact-work-tabel/contact-work-tabel?dataInfo=`+JSON.stringify(datas),
        success: (res) => {

        },
        fail: (res) => {
          console.log(res);
        },
      });
    }

})