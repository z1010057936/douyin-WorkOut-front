var publicFunction = require('../../../../../utils/publicFunction');
import {
  searchContactHistory,
  contactManager
} from "../../../../../api/common";
Page({
  data: {
    concatList: [],
    // 字典值
    dict: {},
    tabelHeads: [
      {
        prop: 'contactTime',
        width: 240,
        label: '联系时间',
        color: '#55C355'
      },
      {
        prop: 'contactFlag',
        width: 200,
        label: '联系结果标志',
      },
      {
        prop: 'contactResult',
        width: 320,
        label: '联系结果',
      },
    ]
  },
  onLoad: function (options) {
    const cusId = options.cusId
    const dictList = tt.getStorageSync("dict");
    this.setData({
      dict: {
        contactFlag: dictList.publicDict.contactFlag
      }
    })
    this.searchContactHistory(cusId)
  },

  async searchContactHistory(cusId) {
    // console.info("cusId",cusId)
    const res = await searchContactHistory({"cusId":cusId})
    if (!res) {
      return;
    }else {
      if(0 === res.err_no) {
        this.setData({
          concatList:res.data
        })
      } else {
        tt.showToast({title: res.err_msg,icon:"fail"});
      }
    }
  }
})