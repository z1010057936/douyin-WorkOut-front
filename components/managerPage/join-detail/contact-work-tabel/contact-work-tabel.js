var publicFunction = require('../../../../utils/publicFunction');
import {
  editJoinsUserInfo
} from "../../../../api/common";
Page({
  data: {
    dataInfo: null, // 数据
    
    dict: {}, // 字典
    contactFlagIndex: 0, // 联系结果下拉框下标
    contactFlag:[], // 联系结果下拉框选项
    resultForm:{}, // 联系结果表单，提交用的
    textMaxLength: 400,// 联系结果输入上限字数
    nowTextLength: 0, // 联系结果当前已输入字数
  },
  onLoad: function (options) {
    // console.info(options.dataInfo)
    let dataInfo = JSON.parse(options.dataInfo);
    const dictList = tt.getStorageSync("dict");
    const { sex } = dictList.publicDict;
    const { contactFlag } = dictList.publicDict;
    const { bodyStatus } = dictList.publicDict;
    
    // 为联系结果下拉框转换字典数据
    let contactFlagArr = [];
    let whileFlag = true;
    let index = 0;
    while(whileFlag) {
      if(undefined !== contactFlag[index]) {
        contactFlagArr[index] = contactFlag[index]
        index ++;
      }else{
        whileFlag = false
      }
    }
    // console.info(dataInfo.contactFlag)
    this.setData({
      dataInfo, // 页面显示数据
      contactFlag: contactFlagArr, // 联系结果标志下拉选项
      contactFlagIndex: dataInfo.contactFlag, // 当前联系结果标志
      // 塞入字典值
      dict: {
        sex,
        contactFlag,
        bodyStatus
      },
      // 上送的数据
      resultForm: {
        cusId: dataInfo.cusId,
        postId: dataInfo.postId,
        contactFlag: dataInfo.contactFlag,
        contactResult: dataInfo.contactResult
      }
    })
  },

  async submitResult(e) {
    // console.info(this.data.resultForm)
    if(!this.isNull(this.data.resultForm.contactFlag) 
        || !this.isNull(this.data.resultForm.contactResult)) {
      tt.showToast({
        title:"请完整填写数据",
        icon: "fail"
      })
      return
    }

    tt.showLoading({title: "请稍后..."});
    const res = await editJoinsUserInfo(this.data.resultForm)
    if (!res) {
      tt.showToast({title:"服务调用故障",icon:"fail"})
      return;
    }else {
      if(0 === res.err_no) {
        tt.hideLoading({});
        tt.showToast({title:"保存成功"})
        const postId = this.data.dataInfo.postId
        setTimeout(() => {
          let pages = getCurrentPages();
          let beforePage = pages[pages.length -2];
          beforePage.searchJoinsUserList({"postId":postId});
          tt.navigateBack({delta: -1})
        }, 1000);
      }else{
        tt.hideLoading({});
        tt.showToast({title:res.err_msg,icon:"fail"})
      }
    }
    
  },

  /**
   * 非空校验
   * @param {*} value 
   */
  isNull(value) {
    if(undefined === value || null === value || '' === value || 0 === value.length) {
        return false
    }
    return true
},

  searchContactHistory(e) {
    let cusId = e.currentTarget.dataset.cusid
      tt.navigateTo({
        url: `/components/managerPage/join-detail/contact-work-tabel/contact-history/contact-history?cusId=`+cusId,
        success: (res) => {
          
        },
        fail: (res) => {
          console.log(res);
        },
      });
  },

  //调用用户拨打电话功能
  tapMakePhoneCall(e) {
    publicFunction.tapMakePhoneCall(e)
  },

  /**
   * 联系结果标志下拉框选择后触发
   * @param {*} e 
   */
  contactFlagChange(e) {
    this.setData({
      contactFlagIndex: e.detail.value,
    })
    this.data.resultForm.contactFlag = e.detail.value
  },

  /**
   * 联系结果输入时触发
   * @param {*} e 
   */
  textCounts(e) {
    this.setData({
      nowTextLength: e.detail.cursor,
    })
    this.data.resultForm.contactResult = e.detail.value
  },

  /**
   * 查看报名职位
   * @param {*} e 
   */
  gotoJoinPostList(e) {
    // console.info(e)
    const cusId = e.target.dataset.cusid;
    // 进行跳转
    tt.navigateTo({
      url: `/pages/custom-joinLog/custom-joinLog?cusId=`+cusId,
      success: (res) => {
        
      },
      fail: (res) => {
        console.log(res);
      },
    });
  }


})