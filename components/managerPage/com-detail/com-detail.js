import {
  searchComDetail,
  comDetailManager
} from "../../../api/common";
Page({
  data: {
    comList: [],
    // 字典值
    dict: {
      status:{0:"草稿",1:"已发布"}
    },
    //表头
    tabelHeads:[
      {
        prop: 'name',
        width: 250,
        label: '企业名称',
      },
      {
        prop: 'status',
        width: 250,
        label: '发布状态',
        color: '#55C355'
      },
      {
        prop: 'sortFlag',
        width: 250,
        label: '排序',
      },
    ],
  },
  onLoad: function (options) {
    this.searchComDetail()
  },
  comDelete(e) {
    // 选择的数据
    const deleteChoose = e.detail;
    if('{}' == JSON.stringify(deleteChoose)) {
      tt.showToast({title:"未选择数据",icon:"fail"})
      return
    }
    // 循环取出key
    // 最终要删除的key数组
    let deleteArr = []
    let deleteChooseKeys = Object.keys(deleteChoose)
    deleteChooseKeys.forEach(element => {
      deleteArr.push(deleteChoose[element].comId)
    })
    tt.showModal({
      title:"删除",
      content:"确定要删除吗",
      success: (res) => {
        if (res.confirm) {
          this.comDeleteImpl(deleteArr)
        }
      },
    });
  },
  /**
   * 删除企业信息
   * @param {*} e 
   */
   async comDeleteImpl(deleteArr) {
    const res = await comDetailManager({"comIds":deleteArr,"optFlag":1})
    if(0 === res.err_no) {
      tt.showToast({title:"删除成功"})
      this.searchComDetail(true)
    }else{
      tt.showToast({title:res.err_msg,icon:"fail"})
    }
    
  },
  /**
   * 增加企业信息，跳转页面进行新增
   * @param {*} e 
   */
  comAdd(e) {
    tt.navigateTo({
      url: "/components/managerPage/com-detail/com-work-tabel/com-work-tabel?flag=add",
      fail: (res) => {
        console.log(res);
      },
    });
  },
  /**
   * 列表点击后触发的方法
   * @param {*} options 
   */
  onRowClick(e) {
    let datas = e.detail.currentTarget.dataset.it
    tt.navigateTo({
      url: "/components/managerPage/com-detail/com-work-tabel/com-work-tabel?flag=edit&dataInfo="+JSON.stringify(datas),
      fail: (res) => {
        console.log(res);
      },
    });
  },

  /**
   * 企业信息列表查询
   */
  async searchComDetail(e) {
    tt.showLoading({title: "请稍后..."});
    const res = await searchComDetail({});
    if(0 === res.err_no) {
      let comList = res.data
      this.setData({
        comList
      })
      // 如果是由子页面触发，则修改Storage缓存信息
      if(e) {
        let dict = tt.getStorageSync("dict");
        let comArr = []
        let comArrOfDict = {}
        for (let index = 0; index < comList.length; index++) {
          const element = comList[index];
          comArr.push({"comId":element.comId,"name":element.name})
          comArrOfDict[element.comId] = element.name
        }
        dict.comArr = comArrOfDict
        
        tt.setStorageSync("comArr", comArr)
        tt.setStorageSync("dict", dict);
      }
    }else {
      tt.showToast({title:res.err_msg, icon:"fail"})
    }
    tt.hideLoading({});
  }
})