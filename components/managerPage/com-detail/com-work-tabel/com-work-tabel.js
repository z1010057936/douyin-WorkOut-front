var publicFunction = require('../../../../utils/publicFunction');
var validate = require("../../../../utils/validata");
const app = getApp();
import {
  comDetailManager,
  comResultAndSortManager
} from "../../../../api/common";
Page({
  data: {
    iPhoneBottomHeight: null,
    dataInfo: null, // 数据
    defaultComPhone: "",
    dict: {}, // 字典
    sortFlagTemp: null, // 临时存储排序标志
    flag: null,
    sortAndStatusHiddenFlag: false,
    swiperImg: [],
    initValidate: {
      name: {
        required: '请填写企业名称',
        comName: '请输入正确的企业名称'
      },
      fullAddress: {
        address: '请输入正确的企业详细地址'
      },
      address: {
        required: '请填写企业地址',
        address: '请输入正确的企业地址'
      },
      longitude: {
        floatStr: '请输入正确的企业坐标(经度)'
      },
      latitude: {
        floatStr: '请输入正确的企业坐标(维度)'
      },
  },
},
  onLoad: function (options) {
    const { systemInfo } = app.globalData;
      
    const dataInfo = undefined == options.dataInfo?{}:JSON.parse(options.dataInfo);
    const sysConfig = tt.getStorageSync("sysConfig");
    const flag = options.flag
    if("add" === flag) {this.setData({sortAndStatusHiddenFlag:true})}
    let {defaultComPhone} = sysConfig.keyAndValue
    let swiperImg = undefined == dataInfo.bannerImgUrl?[]:dataInfo.bannerImgUrl.split(",")

    this.setData({
      dataInfo, // 页面显示数据
      defaultComPhone,
      iPhoneBottomHeight:systemInfo.screenHeight - systemInfo.safeArea.bottom,
      flag,
      swiperImg
    })
  },

  /**
   * 将图片地址保存至企业数据中
   */
  saveImgPathToComInfo(e) {
    console.info("e",e)
  },

  /**
   * 提交接口
   * @param {*} e 
   */
  async formSubmit(e) {
    let datas = e.detail.value
    if(validate.initFormRule(datas, this.data.initValidate) === false) {
      return
    }

    tt.showLoading({title: "请稍后..."});
    // 如果传入企业号码为空则使用配置中的默认电话
    datas.comId = this.data.dataInfo.comId

    const res = await comDetailManager(datas)
    if (!res) {
      tt.showToast({title:"服务调用故障",icon:"fail"})
      return;
    }else {
      if(0 === res.err_no) {
        tt.hideLoading({});
        tt.showToast({title:"保存成功"})
        
        setTimeout(() => {
          let pages = getCurrentPages();
          let beforePage = pages[pages.length -2];
          beforePage.searchComDetail(true);
          if("edit" === this.data.flag) {
            tt.navigateBack({delta: -1})
          }else if("add" === this.data.flag) {
            let dataInfo = res.data
            this.setData({
              sortAndStatusHiddenFlag:false,
              dataInfo
            })
          }
        }, 1000);
      }else{
        tt.hideLoading({});
        tt.showToast({title:res.err_msg,icon:"fail"})
      }
    }
    
  },

  /**
   * 非空校验
   * @param {*} value 
   */
  isNull(value) {
    if(undefined === value || null === value || '' === value || 0 === value.length) {
        return false
    }
    return true
},

/**
 * 修改发布状态
 * @param {*} e 
 */
changeStatus(e) {
  let status = e.currentTarget.dataset.status
  tt.showModal({
    title: "请确认",
    content: status === "0"?"确认要停止发布吗":"确认要发布吗",
    success: (res) => {
      if(res.confirm) {
        this.statusChange(status)
      }
    },
    fail: (res) => {
      
    },
  });
},
/**
 * 当排序标志输入框失去焦点时
 */
 changeSortFlag(e) {
  let sortFlagTemp = e.detail.value
  this.setData({
    sortFlagTemp
  })
},

/**
 * 修改发布状态
 * @param {*} status 
 */
async statusChange(status) {

  let titleMsg = status === "0"?"停止发布":"发布";

  let datas = {
    status: status,
    comId: this.data.dataInfo.comId,
    sortFlag: status === "0"?null:this.data.sortFlagTemp
  }
  const res = await comResultAndSortManager(datas)
  if (!res) {
    tt.showToast({title:"服务调用故障",icon:"fail"})
    return;
  }else {
    if(0 === res.err_no) {
      tt.hideLoading({});
      tt.showToast({title: titleMsg+"成功"})
      // this.sortFlagChange()
      setTimeout(() => {
        let pages = getCurrentPages();
        let beforePage = pages[pages.length -2];
        beforePage.searchComDetail(true);
        tt.navigateBack({delta: -1})
      }, 1000);
    }else{
      tt.hideLoading({});
      tt.showToast({title:res.err_msg,icon:"fail"})
    }
  }
},

/**
 * 
 */
async sortFlagChange() {
  let datas = {
    optFlag: "1",
    sortFlag: this.data.dataInfo.sortFlag,
    comId: this.data.dataInfo.comId
  }
  // const res = await comResultAndSortManager(datas)
}

})