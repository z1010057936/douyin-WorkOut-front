var publicFunction = require('../../../utils/publicFunction');
var validate = require("../../../utils/validata");
import {
  searchSysConfig,
  sysConfigManager
} from "../../../api/common";
const app = getApp();
Page({
  data: {
    iPhoneBottomHeight:null,
    addOrEidtMask: false, // 是否开启蒙层
    maskInfo:{}, // 蒙层表单数据
    maskBtnLoading: false, // 蒙层表单按钮加载动画
    settingsList: [], // 查询回来的数据信息
    maskKeyDisabledFlag: false, // 蒙层中的“键”可否输入开关
    // 字典值
    dict: {},
    //表头
    tabelHeads:[
      {
        prop: 'key',
        width: 410,
        label: '键',
      },
      {
        prop: 'name',
        width: 300,
        label: '名称',
      },
      {
        prop: 'value',
        width: 500,
        label: '值',
      },
      {
        prop: 'context',
        width: 300,
        label: '备注',
      }
    ],
    initValidate: {
      name: {
        required: '请填写名称',
      },
      key: {
        required: '请填写键',
      },
  },
  },
  onLoad: function (options) {
    const { systemInfo } = app.globalData;
    this.setData({
      iPhoneBottomHeight:systemInfo.screenHeight - systemInfo.safeArea.bottom,
    })
    this.getSysInfo()
  },

  /**
   * 获取配置信息
   */
  async getSysInfo(e) {
    tt.showLoading({title: "请稍后...",});
    const sysRes = await searchSysConfig({});
    if(!sysRes) {
      tt.showToast({title:"系统配置查询失败",icon:"fail"})
    }else{
      if(0 === sysRes.err_no) {
        this.setData({
          settingsList: sysRes.data.all
        })
        if(e) {
          tt.setStorageSync("sysConfig", sysRes.data);
        }
      }else {
        tt.showToast({title:sysRes.err_msg,icon:"fail"})
      }
    }
    tt.hideLoading({});
    
  },

  /**
   * 点击数据行
   * @param {*} e 
   */
  onRowClick(e) {
    this.setData({
      maskInfo: {},
      maskKeyDisabledFlag: true
    })
    let thisData = e.detail.currentTarget.dataset.it
    this.setData({
      addOrEidtMask: true,
      maskInfo: thisData
    })
    this.data.maskInfo.optFlag = 1
  },

  /**
   * 删除按钮
   * @param {*} e 
   */
  sysDelete(e) {
    const deleteChoose = e.detail
    // 删除的标志
    if('{}' == JSON.stringify(deleteChoose)) {
      tt.showToast({title:"未选择数据",icon:"fail"})
      return
    }
    // 循环取出key
    // 最终要删除的key数组
    let deleteList = []
    let deleteChooseKeys = Object.keys(deleteChoose)
    deleteChooseKeys.forEach(element => {
      deleteList.push(deleteChoose[element].key)
    });
    let formInfo = {
      "optFlag": 2,
      "deleteList":deleteList
    }
    this.sysConfigManagerImpl(formInfo)
  },

  /**
   * 添加按钮
   * @param {*} e 
   */
  sysAdd(e) {
    this.setData({
      maskInfo: {},
      addOrEidtMask: true,
      maskKeyDisabledFlag: false
    })
    this.data.maskInfo.optFlag = 0
  },

  /**
   * 蒙层表单按钮区-取消
   * @param {*} e 
   */
   contextCancel(e) {
    this.setData({
      addOrEidtMask: false
    })
  },

  /**
   * 蒙层表单按钮区-保存
   * @param {*} e 
   */
   contextSubmit(e) {
     // 数据处理
     let formInfo = e.detail.value
     formInfo.optFlag = this.data.maskInfo.optFlag
     // 数据校验
     if(validate.initFormRule(formInfo, this.data.initValidate) === false) {
      return
    }

     this.setData({
      maskBtnLoading: true
    })
     tt.showLoading({
      title: "请稍后...",
      mask: true
    });
    this.sysConfigManagerImpl(formInfo)
   },

   /**
   * 参数配置管理
   * @param {*} datas 
   */
  async sysConfigManagerImpl(datas) {
    const res = await sysConfigManager(datas)
    publicFunction.baseImplToast(res, "操作成功")
    setTimeout(() => {
      tt.hideLoading({})
      this.getSysInfo(true);
      this.setData({
        addOrEidtMask: false,
        maskBtnLoading: false,
        maskInfo: {}
      })
    }, 1000);
  },
})