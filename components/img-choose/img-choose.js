import {
  comImgUploader
} from "../../api/common";
import { config } from  "../../api/config"
Component({
  data: {
    imgUploadLoading: false, // 图片上传按钮加载动画
    imageList: [], // 图片选择列表
    countIndex: 4,
    count: [1, 2, 3, 4, 5],
    swiperImg: [], // 已在库中的图片地址
    fileBasePath: "", // 文件基础地址，从config.js中取
    uploadPercentFlag: false, // 上传进度条显示开关
    uploadPercent:0, // 图片上传进度
  },
  properties: {
    swiperImg: {
      type: Array,
    },
  },
  attached(options) {
    let fileBasePath = config.fileBasePath
    this.setData({
      fileBasePath
    })
  },
  methods: {

    /**
     * 清空选择的图片列表
     * @param {*} e 
     */
    clearImg(e) {
      this.setData({
        imageList: []
      })
    },
    /**
     * 已选择图片预览
     * @param {*} e 
     */
    previewImage(e) {
      var current = e.target.dataset.src;
      tt.previewImage({
        current: current,
        urls: this.data.imageList
      });
    },

    /**
     * 选择图片
     */
    chooseImage() {
      var that = this;
      tt.chooseImage({
        sourceType: 'album',
        sizeType: 'original & compressed',
        count: this.data.count[this.data.countIndex] - this.data.imageList.length,
        success(res) {
          console.log(res);
          const imageList = that.data.imageList.concat(res.tempFilePaths)
          that.setData({
            imageList
          });
        },
        fail(res) {
          tt.showToast({
            title: res.errMsg
          });
        },
        complete: res => {
          // console.log(res, "选择图片complete已触发");
        }
      });
    },

    /**
     * 点击图片上传按钮
     */
    uploadImgHandler() {
      this.setData({
        imgUploadLoading: true,
        uploadPercentFlag: true
      })
      let successUpload = [] // 成功上传的文件列表
      let path = "" // 拼装的文件上传路径

      for (let index = 0; index < this.data.imageList.length; index++) {
        const imgUrl = this.data.imageList[index];
        path = "comBannerImg/" + imgUrl.substr(imgUrl.indexOf('temp/')+5,imgUrl.length);
        const uploadFlag = this.uploadImg(path, imgUrl)
        if(uploadFlag) {
          successUpload.push(path)
        }

        // 根据文件数量计算上传进度
        let uploadPercent = ((index+1)/this.data.imageList.length) * 100
        console.info("uploadPercent", uploadPercent)
        this.setData({
          uploadPercent
        })
      }

      // this.data.imageList.forEach(imgUrl => {
      //   path = "comBannerImg/" + imgUrl.substr(imgUrl.indexOf('temp/')+5,imgUrl.length);
      //   const uploadFlag = this.uploadImg(path, imgUrl)
      //   if(uploadFlag) {
      //     successUpload.push(path)
      //   }
      //   tt.setData({
      //     uploadPercent: this.data.imageList.length
      //   })
      // });
      this.triggerEvent("uploadOver", {"dataList": successUpload} );
      this.setData({
        imgUploadLoading: false
      })
    },

    /**
     * 图片上传至抖音云存储
     * @param {*} path  拼装的文件上传路径
     * @param {*} imgUrl 文件地址
     */
     async uploadImg(path, imgUrl) {
       
      // const res = await comImgUploader(path, imgUrl)
      const res = {errMsg: "Cloud.uploadFile:ok"}
      if(-1 !== res.errMsg.indexOf("ok")) {
        return true
      }else {
        return false
      }
    },

    /**
     * 将抖音云存储中图片所在路径，保存至该企业的数据中
     */
    saveImgUrl() {

    }



  }
})