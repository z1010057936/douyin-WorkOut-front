// d:\dev\code\douyin-WorkOut-front\components\post-label\post-label.js
Component({
  data: {
    labelArry: [],
  },
  properties: {
    postName: String,
    labelList: String,
    wantLength:Number,
    mainLabel:String
  },
  methods: {
   
  },
  attached() {
    //处理传入的标签数据
    var label = this.properties.labelList;
    var length = this.properties.wantLength;
    var labelArryTemp = [];
    //如果未传入想要的长度，则全部分割
    if(0 === length) {
      labelArryTemp = label.split(",")
    }else {
      labelArryTemp = label.split(",",length)
    }
    this.setData({
      labelArry: labelArryTemp
    })
  }
})