/**
 * 表单验证
 * 
 * @param {Object} rules 验证字段的规则
 * @param {Object} messages 验证字段的提示信息
 * 
 */
export {initFormRule}

let rulerReg = {
    cc: /^[\u4E00-\u9FA5]{2,10}(·[\u4E00-\u9FA5]{2,10}){0,2}$/,
    idcard: /^([1-6][1-9]|50)\d{4}(18|19|20)\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/,
    email: /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
    tel: /^1[34578]\d{9}$/,
    url: /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i,
    number: /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/,
    salary: /^[1-9]\d*[-][1-9]\d*[/][\u6708\u65e5]$/,
    comName: /^(?!\s*$)[A-Za-z0-9.·_\-\(\)\u4e00-\u9fa5\（）\s-]+$/,
    address: /^(?!\s*$)[A-Za-z0-9.·_\-\(\)\u4e00-\u9fa5\（）\s-]+$/,
    floatStr: /^-?\d+(\.\d+)?$/,
}

let errorMsg = []


/**
 * 校验的入口方法
 * @param {*} value 传入的表单数据
 * @param {*} initValidate 传入的校验规则json
 */
function initFormRule(value, initValidate) {
    // console.info(value, initValidate)
    errorMsg = []
    const keys = Object.keys(initValidate);
    keys.forEach(function (item, index) {
        checkRule(item,value[item],initValidate[item])
    })

    if(errorMsg.length > 0) {
        showMsg()
        return false;
    }
    return true;
}

/**
 * 执行该字段对应的校验规则
 * @param {*} name 字段名称
 * @param {*} value 字段值
 * @param {*} ruler 字段的校验规则
 */
function checkRule(name,value,ruler) {
    // console.info(name,value,ruler)
    if(undefined === ruler) {return}
    const rulerKeys = Object.keys(ruler);
    rulerKeys.forEach(function (item, index) {
        let isNullResult = isNullResult = isNull(value);
        if("required" === item && false === isNullResult) {
            errorMsg.push(ruler[item])
        }
        if(undefined !== rulerReg[item] && isNullResult) {
            regTest(value, rulerReg[item], ruler[item])
        }
        
    })
}

/********具体的规则校验***** */
/**
 * 字段非空校验
 * @param {*} value 字段值
 * @param {*} msg 字段非空提示语
 */
function isNull(value) {
    // console.info(value, msg)
    if(undefined === value || null === value || '' === value || 0 === value.length) {
        return false
    }
    return true
}

/**
 * 字段的正则表达式校验
 * @param {*} name 字段名
 * @param {*} value 字段值
 * @param {*} needRule 需要用到的正则规则
 * @param {*} msg 字段不符合校验规则时的提示语
 */
function regTest(value, needRule, msg) {
    // console.info(value, needRule, msg,needRule.test(value))
    if(false === needRule.test(value)) {
        errorMsg.push(msg)
    }
}

/**
 * 展示信息弹框
 */
function showMsg() {
    // console.info(errorMsg)
    tt.showModal({
        title: "错误",
        content: errorMsg[0],
        showCancel: false
    });
}