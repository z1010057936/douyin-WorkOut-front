import {
  customJoinLogManager,
  getCusUnionid,
  customResumeInit,
  writePoint,
  dyDecrypt
} from "../api/common";
import {
  config
} from "../api/config";

let datas = {
  ruleParams: {
    "name": "姓名",
    "sex": "性别",
    "bodyStatus": "健康状况",
    "idno": "身份证号",
    "phoneNum": "手机号",
    "wantPost": "期望职位",
    "wantCity": "期望城市",
    "wantSalary": "期望薪资"
  },
  imgLocalStorageDatas: tt.getStorageSync("imgDatas"), // 图片缓存对象
  fileBasePath: config.fileBasePath
}

export {
  tapMakePhoneCall,
  joinPost,
  formRule,
  customJoinLogManagerInterFace,
  getCusUnionidOfDouyin,
  isEmpty,
  baseImplToast,
  getRandom,
  getRandomNorepeatNumber,
  douyinLogin,
  douyingetUserProfile,
  douyingetPhoneNum,
  resumeInit,
  imgGetLocalStorage,
  setStorageSyncSecond,
  getStorageSyncTime
}

/**
 * 获取期望个数的指定范围随机数
 * @param {*} count 
 * @param {*} min 
 * @param {*} max 
 */
function getRandomNorepeatNumber(count, min, max) {
  let numberList = []
  for (let index = 0; index < count; index++) {
    const number = getRandom(min, max)
    if (-1 === numberList.indexOf(number)) {
      numberList.push(number)
    } else {
      index--
    }
  }
  return numberList
}


/**
 * 取指定范围内的随机数
 * @param {*} min 期望的最小值
 * @param {*} max 期望的最大值
 */
function getRandom(min, max) {
  return Math.floor(Math.random() * ((max - min) + min)); //常用
}
/**
 * 非空判断
 * @param {*} data 
 */
function isEmpty(data) {
  if (undefined === data || null === data || '' === data || 0 === data.length) {
    return true
  }
  return false
}

/**
 * 最基础的接口消息提示
 * @param {*} data 接口返回数据
 * @param {*} msg 自定义的成功消息提示
 */
function baseImplToast(data, msg) {
  if (!data) {
    return false;
  } else {
    if (0 === data.err_no) {
      tt.showToast({
        title: msg
      });
      return true;
    } else {
      tt.showToast({
        title: data.err_msg,
        icon: "fail"
      });
      return false;
    }
  }
}

/**
 * 调用用户拨打电话功能
 * @param {*} e 
 */
function tapMakePhoneCall(e) {
  let phoneNumber = e.currentTarget.dataset.comphonenum
  //  console.info(e)
  if (isEmpty(phoneNumber)) {
    const sysConfig = tt.getStorageSync("sysConfig");
    phoneNumber = sysConfig.keyAndValue.defaultComPhone
  }
  tt.makePhoneCall({
    phoneNumber: phoneNumber
  });
}

/**
 * 立即报名前置接口，账号状态
 * @param {*} e 
 */
function joinPost(e) {
  //判断是否登录
  const isLogin = tt.getStorageSync("isLogin");

  if (isLogin == true) {
    const customBaseInfo = JSON.parse(tt.getStorageSync("customBaseInfo"));
    const ruleResult = formRule(customBaseInfo)
    var {
      cusId
    } = customBaseInfo;
    var {
      postid
    } = e.currentTarget.dataset;
    let data = {
      cusId,
      postId: postid,
      optFlag: 0
    }
    // console.log(ruleResult)
    // 如果简历信息完整，则允许立即报名操作，否则提示需要补充
    if (ruleResult) {
      tt.showModal({
        content: "确定要报名该岗位吗？",
        confirmText: "确定",
        cancelText: "关闭",
        success(res) {
          if (res.confirm) {
            customJoinLogManagerInterFace(data, customBaseInfo)
          }
        }
      });
    } else {
      tt.showModal({
        content: "简历信息不完整，请先补充完成",
        confirmText: "确定",
        cancelText: "关闭",
        success(res) {
          if (res.confirm) {
            tt.navigateTo({
              url: `/pages/resume/resume-form`,
              success: (res) => {

              },
              fail: (res) => {
                console.log(res);
              },
            });
          }
        }
      })
    }
  } else {
    tt.showToast({
      title: "请先登陆后再继续",
      icon: "fail"
    });
  }

}

//表单校验
function formRule(e) {
  if (isEmpty(e.phoneNum)) {
    return false;
  }
  if (isEmpty(e.sex)) {
    return false;
  }
  if (isEmpty(e.name)) {
    return false;
  }
  return true;
}


// 触发报名接口
async function customJoinLogManagerInterFace(data, customBaseInfo) {
  //这里调用后台报名接口
  const res = await customJoinLogManager(data)
  if (0 === res.err_no) {
    tt.showToast({
      title: '报名成功'
    });
    let pointData = {
      name: customBaseInfo.name,
      sex: customBaseInfo.sex,
      phoneNum: customBaseInfo.phoneNum,
    }
    // console.info(pointData)
    await writePoint({
      "pointCode": "joinPostPoint",
      "datas": JSON.stringify(pointData)
    });
    console.info("触发埋点-立即报名，用户Id:", data.cusId, " 职位id:", data.postId)
  } else {
    tt.showToast({
      title: res.err_msg,
      icon: 'fail'
    });
  }
}

/**
 * 抖音获取用户手机号
 * @param {*} data 加密的用户手机号数据
 */
async function douyingetPhoneNum(data) {
  // 首先需要对data.encryptedData解密
  let {
    errMsg
  } = data.detail
  if (-1 !== errMsg.indexOf("ok")) {
    console.info("----douyingetPhoneNum 执行成功，准备对数据进行解密")
    const ttLoginRes = tt.getStorageSync("ttLoginRes");
    return await dyDecryptFuc(data.detail.encryptedData, data.detail.iv, ttLoginRes.code, ttLoginRes.anonymous_code)
  } else if (-1 !== errMsg.indexOf("fail auth deny")) {
    tt.hideLoading({});
    return null
  } else {
    console.info("----douyingetPhoneNum 执行失败：" + errMsg)
    tt.hideLoading({});
    tt.showToast({
      title: errMsg,
      icon: 'fail'
    });
    return null
  }


}

/**
 * 抖音获取用户信息(暂时弃用，已替换为手机号授权登录)
 */
function douyingetUserProfile(that) {
  tt.showLoading({
    title: "请稍等...",
    mask: true
  });
  tt.getUserProfile({
    success(res) {
      console.info("----tt.getUserProfile 执行成功")
      getCusUnionidOfDouyin(that)
    },
    fail(res) {
      console.error("getUserProfile 调用失败", res);
      tt.hideLoading({});
    },
  })
}

// 抖音一键登录
function douyinLogin() {
  tt.checkSession({
    success: (res) => {
      console.info("----tt.checkSession 执行成功，Session具有时效性")
      const isLogin = tt.getStorageSync("isLogin");
      const ttLoginRes = tt.getStorageSync("ttLoginRes")
      if (false === isLogin || isEmpty(ttLoginRes)) {
        console.info("----tt.checkSession 但是登录重要凭证为空，需要重新执行tt.login")
        ttLogin()
      }
    },
    fail: (res) => {
      console.info("----tt.checkSession 执行成功，Session已失效，执行tt.login")
      ttLogin()
    }
  })
}
/**
 * 单独将登录拿出来，避免代码重复
 */
function ttLogin() {
  tt.login({
    success: (res) => {
      console.info("----tt.login 执行成功")
      // this.getCusUnionidOfDouyin(res.code, res.anonymousCode)
      tt.setStorageSync("ttLoginRes", {
        "code": res.code,
        "anonymous_code": res.anonymousCode
      });
    },
    fail: (res) => {
      console.info("----tt.login 执行失败：" + res)
      tt.hideLoading({});
      // tt.showToast({title:"抖音一键登录失败，请尝试其他方式",icon:"fail"})
    },
  });
}

/**
 * 通过抖音服务器的code2Session获取uinionid(用户抖音平台唯一ID)
 * @param {*} code 临时登录凭证, 有效期 5 分钟。开发者可以通过在服务端调用 登录凭证校验接口 换取 openid 和 session_key 等信息。
 * @param {*} anonymousCode 用于标识当前设备, 无论登录与否都会返回, 有效期 5 分钟。
 */
async function getCusUnionidOfDouyin(code, anonymousCode) {
  let data = {
    appid: config.appID,
    secret: config.appSecret,
    code: code,
    anonymous_code: anonymousCode
  }
  const cusUnionidRes = await getCusUnionid(data)

  if (0 === cusUnionidRes.err_no) {
    console.info("----code2Session 执行成功")
    // 如果获取成功，则通过cusId获取用户信息
    const cusId = cusUnionidRes.data.unionid.replaceAll("-", "");
    const session_key = cusUnionidRes.data.session_key
    const openid = cusUnionidRes.data.openid

  } else {
    console.info("----code2Session 执行失败：" + res.err_msg)
    tt.showToast({
      title: res.err_msg,
      icon: 'fail'
    });
  }
  tt.hideLoading({});
}

/**
 * 抖音敏感数据解密，其中已包含调用抖音服务器的code2Session
 * @param {*} encryptedData 包括敏感数据在内的完整用户信息的加密数据
 * @param {*} iv 加密算法的初始向量
 * @param {*} code 临时登录凭证, 有效期 5 分钟。开发者可以通过在服务端调用 登录凭证校验接口 换取 openid 和 session_key 等信息。
 * @param {*} anonymousCode 用于标识当前设备, 无论登录与否都会返回, 有效期 5 分钟。
 */
async function dyDecryptFuc(encryptedData, iv, code, anonymousCode) {
  let data = {
    appid: config.appID,
    secret: config.appSecret,
    encryptedData,
    iv,
    code,
    anonymousCode
  }

  const res = await dyDecrypt(data);
  if (0 === res.err_no) {
    console.info("----tt.dyDecryptFuc 执行成功")
    const resData = res.data
    const cusId = resData.unionid.replaceAll("-", "");
    const phoneNum = resData.phoneNumber

    return {
      "cusId": cusId,
      "phoneNum": phoneNum
    }

  } else {
    console.info("----tt.dyDecryptFuc 执行失败:" + res.err_msg)
    tt.hideLoading({});
    tt.showToast({
      title: res.err_msg,
      icon: 'fail'
    });
    return null
  }
}



/**
 * 简历信息初始化
 * @param {*} data.cusId 客户ID 通过code2Session获取的uinionid
 * @param {*} data.phoneNum 手机号
 */
async function resumeInit(data, that) {
  let customBaseInfo = {}
  const customResumeRes = await customResumeInit({
    "cusId": data.cusId,
    "phoneNum": data.phoneNum,
    "optFlag": 0
  });
  // 如果查询数据不为空，则将其赋值给customResumeRes用于后续业务及展示，否则只塞入cusId
  if (0 === customResumeRes.err_no) {
    // 是否为新用户
    // 判断是否为空数据
    if (null === customResumeRes.data) {
      customBaseInfo.cusId = cusId
    } else {
      customBaseInfo = customResumeRes.data
    }
    // TODO: 后续需要迁移到手机号码解密的代码块内，并且增加phoneNum上送
    await writePoint({
      "pointCode": "loginPoint",
      "datas": JSON.stringify({
        "name": customBaseInfo.name,
        "phoneNum": customBaseInfo.phoneNum
      })
    });
    tt.showToast({
      title: "登录成功",
      icon: 'success'
    });
  } else {
    tt.showToast({
      title: cusUnionidRes.err_msg,
      icon: 'fail'
    });
  }

  // 将参数们塞入缓存和页面展示用
  if (undefined !== that) {
    that.setData({
      isLogin: true,
      customInfo: customBaseInfo,
      isTiptrue: false
    })
  }

  tt.setStorageSync("isLogin", true);
  tt.setStorageSync("customBaseInfo", JSON.stringify(customBaseInfo));
  console.info("----resumeInit 初始化成功，客户唯一ID:" + data.cusId)
}

/**
 * 图片缓存处理
 * @param {*} path 
 */
async function imgGetLocalStorage(path) {
  if (isEmpty(path)) {
    return
  }
  // 返回的本地路径
  let resultPath = '';

  const imgDatas = datas.imgLocalStorageDatas
  const localPath = imgDatas[path]
  let url = datas.fileBasePath + encodeURI(path)

  // 网络的地址放入对象，以适应异步操作，防止不展示图片，后续访问再使用缓存
  resultPath = url
  return new Promise((resolve, reject) => {
    // 如果没有从缓存中获取图片的本地路径，那么就从云端下载，然后塞进缓存里
    // 否则直接使用本地路径
    if (!localPath) {
      getImgFromCloud(url, path)
      resolve(resultPath)
    } else {
      // console.info("---- Storage缓存已有本地图片地址，直接使用,",path)

      tt.getFileInfo({
        filePath: localPath,
        success: (res) => {
          resultPath = localPath
          resolve(resultPath)
        },
        fail: (res) => {
          // console.info("---- 本地文件缓存已不存在，重新从云端获取,",path)
          getImgFromCloud(url, path)
          resolve(resultPath)
        },
      });

    }
  })
}

/**
 * 从云端获取文件，并放入缓存
 * @param {*} url 
 */
async function getImgFromCloud(url, path) {
  // console.info("---- 前往云端获取图片,",path)
  const imgDatas = datas.imgLocalStorageDatas

  tt.downloadFile({
    url: url,
    success: (res) => {
      imgDatas[path] = res.tempFilePath
      tt.setStorageSync("imgDatas", imgDatas);
    },
    fail: (res) => {
      console.error("downloadFile fail", res);
    },
  });
}

/**
 * ### 设置时效缓存
 * @param  {String} key    存储的key值
 * @param  {String} value  存储的value值 (不填则默认为1)
 * @param  {Number} time   有效时间，（单位：秒，不填则默认一天）
 */
function setStorageSyncSecond(key, value, time) {
  value = value ? value : 1
  tt.setStorageSync(key, value)
  var t = time ? +time : 24 * 3600
  if (t > 0) {
    var timestamp = new Date().getTime()
    timestamp = timestamp / 1000 + t
    tt.setStorageSync(key + 'dtime', timestamp + "")
  } else {
    tt.removeStorageSync(k + 'dtime')
  }
}

/**
 * ### 读取时效缓存
 * @param   {String}  key  存储的key值
 * @return  {*} true为当前时间已失效、或者该值不存在,undefined默认返回则为当前时间未到失效时间
 */
function getStorageSyncTime(key) {
  var deadtime = +tt.getStorageSync(key + 'dtime')
  if (deadtime) {
    if (deadtime < (new Date().getTime()) / 1000) {
      tt.removeStorageSync(key)
      tt.removeStorageSync(key + 'dtime')
      return false
    } else {
      return tt.getStorageSync(key)
    }
  } else {
    return false
  }
}