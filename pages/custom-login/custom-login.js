var Mcaptcha = require('../../utils/mcaptcha.js');
var publicFunction = require('../../utils/publicFunction');
import { customLoginInfoCheck,customLogin} from "../../api/common";
Page({
  data: {
    phoneNumber: null,
    isLogin: false,
    customInfo: {},
    pageType:'login',
    imgCode:null,
    loginHome: true,
    userPhoneLogin: false, //使用手机号登录
    gotoManagerNeetClickCounts: 0,
    loginLoadding: false
  },

  onReady() {
    this.mcaptcha=new Mcaptcha({
      el: 'canvas',
      width: 80,
      height: 35,
      createCodeImg: ""
      });
  },
  onShow(){
    // console.info("loginOnShow",1)
    
    // 检查是否存在登录标志
    var isLoginFlag = tt.getStorageSync("isLogin");
    let customBaseInfo = {}
    if(isLoginFlag) {
      customBaseInfo = JSON.parse(tt.getStorageSync("customBaseInfo"));
      this.setData({
        isLogin: isLoginFlag,
        customInfo: customBaseInfo
      })
    }
  },

  async onLoad() {
    // 检查是否存在登录标志
    // var isLoginFlag = tt.getStorageSync("isLogin");
    // 先用抖音api查询是否有过一键登录
    // tt.checkSession({
    //   success: () => {
    //     isLoginFlag = true
    //   },
    // });
    // let customBaseInfo = {}
    // if(isLoginFlag) {
    //   customBaseInfo = JSON.parse(tt.getStorageSync("customBaseInfo"));
    // }
    // this.setData({
    //   isLogin: isLoginFlag,
    //   customInfo: customBaseInfo
    // })
  },

  gotoLoginHome() {
    this.setData({
      userPhoneLogin: false
    })
  },

  changePhoneLogin(){
    this.setData({
      userPhoneLogin: true
    })
  },

  gotoManagerPage() {
    this.data.gotoManagerNeetClickCounts ++
    if(this.data.gotoManagerNeetClickCounts >= 1) {
      console.info("跳转至管理员界面")
      tt.navigateTo({
        url: `/pages/manager/manager`,
        success: (res) => {
          
        },
        fail: (res) => {
          console.log(res);
        },
      });
    }
  },
  async douyinLogin(e) {
    // 注释 用户信息授权，改为 手机号授权
    // publicFunction.douyingetUserProfile(this);
    // console.info("getPhone",e.detail)
    this.setData({
      loginLoadding: true
    })
    const res = await publicFunction.douyingetPhoneNum(e)
    if(null !== res) {
      publicFunction.resumeInit(res, this)
      this.setData({
        loginLoadding: false
      })
    }else {
      tt.showToast({
        title: "登录失败，请稍后重试",
        icon: "fail"
      });
      this.setData({
        loginLoadding: false
      })
    }

  },
    //刷新验证码
  onTap(){
    this.mcaptcha.refresh();
  },
  
  //立即报名，跳转到简历填写页
  // joinPost() {
  //     tt.navigateTo({
  //       url: `/pages/resume/resume-form`,
  //       success: (res) => {
  //        this.setData({
  //         isLogin: true
  //        })
  //       },
  //       fail: (res) => {
  //         console.log(res);
  //       },
  //     });
  //   },

    //提交（暂时废弃，目前使用抖音一键登录接口）
    async formSubmit(e) {
      if(this.formRule(e.detail.value) === false) {
        return
      }
      //验证验证码
      var macptchaRes = this.mcaptcha.validate(e.detail.value.codeImg);
      if (!macptchaRes) {
        tt.showToast({ title: '图形验证码错误',icon: 'fail'})
        
        return;
      }
      this.onTap()
      let datas = e.detail.value;
      datas.optFlag = 0;

      // 登录前置校验
      // 1. 账户存在检查
      // 2. 密码校验
      // 3. 如若正确，则登录 customLoginInfoCheck方法中已包含登录方法
      // 4. 如账号不存在则触发注册
      const checkRes = await customLoginInfoCheck(datas);
      if(-2 === checkRes.err_no) {
        tt.showModal({
          content: "您的手机号未注册，是否同意授权用于登录？",
          confirmText: "同意",
          cancelText: "不同意",
          success: (res) => {
            if (res.confirm) {
              this.loginInterface(datas)
            }
          },
          fail: (res) => {
            return
          },
        });
      }else if(0 == checkRes.err_no){
        let customBaseInfo = checkRes.data
        this.setData({
          isLogin: true,
          customInfo: customBaseInfo
        })
        tt.setStorageSync("isLogin", true);
        tt.setStorageSync("customBaseInfo", JSON.stringify(customBaseInfo));
      } else {
        tt.showToast({title: checkRes.err_msg, icon: 'fail'});
      }
    },

    // 注册接口
    async loginInterface(datas) {
      const res = await customLogin(datas);
      if(0 === res.err_no) {
        let customBaseInfo = {"phoneNum":datas.phoneNum,"phoneNumhide":datas.phoneNum.substr(7), "cusId":res.data.cusId}
        tt.setStorageSync("isLogin", true);
        tt.setStorageSync("customBaseInfo", JSON.stringify(customBaseInfo));
        // setTimeout(() => {
        //   tt.navigateBack({
        //     delta: 1})
        // }, 1000);
        tt.hideLoading({});
        tt.showToast({
          title: '提交成功',
          icon: 'success'
        });
        this.setData({
          isLogin: true,
          customInfo: customBaseInfo
        })
        
      }else {
        tt.showToast({
          title: res.err_msg,
          icon: 'fail'
        });
      }
    },



    //表单校验
  formRule(e) {
    var result = true;
    const keys = Object.keys(e);
    keys.forEach(function (item,key) {
      if(e[item] === "" || e[item] === null || e[item].length === 0) {
        tt.showToast({title: "请完整填写信息",icon: 'fail'});
        result = false;
      }
    })
    return result;
  },

  /**
   * 退出登录
   */
  signOut() {
    tt.setStorageSync("isLogin", false);
    tt.setStorageSync("customBaseInfo", JSON.stringify({}));
    tt.setStorageSync("ttLoginRes", JSON.stringify({}));
    tt.removeStorageSync("ttLoginRes");
    this.setData({
      isLogin: false,
      customInfo: {}
    })
    // 刷新登录凭证，以防用户又想登录
    publicFunction.douyinLogin()
  }

});
