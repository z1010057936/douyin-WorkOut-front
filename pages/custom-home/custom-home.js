// d:\dev\code\douyin-WorkOut-front\pages\custom-home\custom-home.js
Component({
  data: {
    userId: "",
  },
  properties: {
    customInfo:{
      type:JSON,
    }
  },
  async attached(e) {
    
  },
  methods: {
    // 跳转简历页面
    gotoResume(e) {
      tt.navigateTo({
        url: `/pages/resume/resume-form`,
        success: (res) => {
          
        },
        fail: (res) => {
          console.log(res);
        },
      });
    },

    //跳转到报名记录页面
    gotoPostDetail(e) {
      tt.navigateTo({
        url: `/pages/custom-joinLog/custom-joinLog?cusId=`+this.properties.customInfo.cusId,
        success: (res) => {
          
        },
        fail: (res) => {
          console.log(res);
        },
      });
    },

    /**
     * 退出登录
     */
    signOut() {
      this.triggerEvent("signOutFunction", {} );
      console.info("----signOut 执行成功，用户账号已登出")
    }

  }
})