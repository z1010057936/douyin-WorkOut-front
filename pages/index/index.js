import {
  getComPostInfo,
  searchSysConfig,
  getDict,
  searchComDetailAndBanners,
  searchComDistributionAddress
} from "../../api/common";
var publicFunction = require('../../utils/publicFunction');
const app = getApp()

Page({
  
  data: {
    sortList: ['热招职位'/*'距离优先', '好评优先', '销量优先'*/],
    current: 0,
    bannerList:[],
    swiperHeight:0,
    distanceSortList:[],
    comInfoList:[],
    postInfoList:[],
    addressInfoList: [],
    viewFlag: "com",
    isFullScreen: false, // 是否全屏
    navHeight: 0, 
    comPageParams: {
      page: 1,
      limit: 10
    },
    postPageParams: {
      page: 1,
      limit: 10
    },
    comMaxPages: 1,
    postMaxPages: 1,
  },
  /**
   * 页面上拉触底事件的处理函数
   * @param {*} e 
   */
  async onReachBottom(e) {
    // 企业视图
    if("com" === this.data.viewFlag) {
      if(this.data.comMaxPages === this.data.comPageParams.page) {
        tt.showToast({
          title: "没有更多数据了",
          icon: "none"
        });
        return
      }
      this.data.comPageParams.page ++
      tt.showLoading({title: "请稍后...",});
      await this.thisGetComInfo()
    }else if ("post" === this.data.viewFlag) {
      // 职位视图
      if(this.data.postMaxPages === this.data.postPageParams.page) {
        tt.showToast({
          title: "没有更多数据了",
          icon: "none"
        });
        return
      }
      this.data.postPageParams.page ++
      tt.showLoading({title: "请稍后...",});
      await this.thisGetComPostInfo()
    }

  },
 async onLoad () {
  const { systemInfo } = app.globalData;
  this.setData({
    navHeight: systemInfo.windowHeight - 110
  })
  
  // console.info("systemInfo",systemInfo)
   // 查询系统基础配置，包含参数配置、字典码
   this.getSysInfo()
   //查询职业列表数据
  //  tt.showLoading({title: "请稍后...",});
  //  this.thisGetComPostInfo();
  },
  /**
   * 监控当前屏幕尺寸
   * @param {*} res 
   */
  onResize(res) {
    // console.info("indexonResize",res)
    if(1 == res.screenRatio) {
      this.setData({
        isFullScreen: true,
        navHeight: res.windowHeight - 300
      })
    }else {
      this.setData({
        isFullScreen: false,
        navHeight: res.windowHeight - 250
      })
    }
  },
  onReady(){
    this.getHeight();
  },
  switchTap(e){
    // 可在此处理选中后更新商品列表数据
    const {current} = e.detail;
    this.setData({
      current,
    })
    this.getHeight()
  },
  getHeight(){
    let that =this;
    tt.createSelectorQuery().select('#card').boundingClientRect(function(rect){
    }).exec(res=>{
      // console.log(res);
      that.setData({
        swiperHeight:res[0].height+''
      })
    });
  },

  /**
   * 切换企业或职位视图
   * @param {*} e 
   */
  async switchView(e) {
    const tittle = e.detail.tittle
    
    var data = {"mainLabel": null}
    let viewFlag = "";
    switch(tittle) {
      case "优秀企业":
        viewFlag = "com"
        break;
      case "好工作":
        viewFlag = "post"
        break;
      case "地区":
        viewFlag = "address"
        break;
      default:

    }

    // 职位视图，查询职位列表
    if("post" === viewFlag) {
      this.setData({
        postInfoList: [],
        postPageParams: {page:1,limit:10}
      })

      await this.thisGetComPostInfo()

      this.setData({
        viewFlag
      })
      // 企业视图，查询企业列表
    }else if("com" === viewFlag){
      this.setData({
        comInfoList: [],
        comPageParams: {page:1,limit:10}
      })
      await this.thisGetComInfo()

      this.setData({
        viewFlag
      })
      // 地区视图，查询地区列表
    }else if ("address" === viewFlag) {
      await this.thisGetAddressInfo()
      
      this.setData({
        viewFlag
      })
    }
  },

  /**
   * 获取地区列表信息
   */
  async thisGetAddressInfo() {
    const addressInfoList = publicFunction.getStorageSyncTime("addressInfoList");
    // 从缓存中取地区列表，如缓存过期，则从接口取，否则直接用
    if(!addressInfoList) {
      const res = await searchComDistributionAddress({});
      if (!res) {
        return;
      }
      tt.hideLoading({});
      this.removeSkeleton && this.removeSkeleton();
      publicFunction.setStorageSyncSecond("addressInfoList",res.data)
      this.setData({
        addressInfoList: res.data
      })
    }else {
      tt.hideLoading({});
      this.setData({
        addressInfoList
      })
    }

    
  },

  // 查询全部职业信息
  async thisGetComPostInfo() {
    const res = await getComPostInfo(this.data.postPageParams);
    // console.info("res:",res)
    if (!res) {
      return;
    }
   const postRes = res.data.records;

   // 获取原职位数组
   let rawPostInfoList = this.data.postInfoList
   // 往职位数组里增加新数据
   rawPostInfoList.push(...postRes)

   tt.hideLoading({});
   this.removeSkeleton && this.removeSkeleton();
    this.setData({
      postInfoList: rawPostInfoList,
      postMaxPages: res.data.pages
    })
  },

  /**
   * 获取企业信息
   */
  async thisGetComInfo() {
    const comRes = await searchComDetailAndBanners(this.data.comPageParams);
    // let comArr = [];
    // let comArrOfDict = {}
    let rawComList = this.data.comInfoList
    if (0 == comRes.err_no) {
      let comList = comRes.data.records;
      // comArr.push({"comId":"","name":"-----"})
      for (let index = 0; index < comList.length; index++) {
        const item = comList[index];
        // comArr.push({"comId":item.comId,"name":item.name})
        // comArrOfDict[item.comId] = item.name

        // 处理图片字符串，并逐个获取本地路径
        let bannerImgUrl = item.bannerImgUrl
        if(null === bannerImgUrl || '' === bannerImgUrl || undefined === bannerImgUrl) {
          continue
        } else {
          // 分割图片地址字符串
          let bannerImgUrlArr = bannerImgUrl.split(',')

          // 从缓存获取图片地址，如果不存在则从云端获取
          for (let index = 0; index < bannerImgUrlArr.length; index++) {
            const item = bannerImgUrlArr[index];
            let localPath = await publicFunction.imgGetLocalStorage(item)
            // console.info("resultPath", localPath)
            bannerImgUrlArr[index] = localPath
          }

          // 将处理过的图片数组塞给原数据
          item.bannerImgUrl = bannerImgUrlArr;
          
        }
      }
      
      tt.hideLoading({});
      this.removeSkeleton && this.removeSkeleton();
      
      // push进列表数组里
      rawComList.push(...comList)
        this.setData({
          comInfoList: rawComList,
          comMaxPages: comRes.data.pages
        })
        // return comArrOfDict
    } else {
      tt.hideLoading({});
      tt.showToast({
        title: comRes.err_msg, icon:"fail"});
    }

    // tt.setStorageSync("comArr", comArr)
  },

  /**
   * 初始化系统参数
   */
  async getSysInfo() {
    // 查询系统参数
    const sysRes = await searchSysConfig({});
    if (!sysRes && 0 !== sysRes.err_no) {
      console.error(sysRes.err_msg)
      // tt.showToast({title:"系统配置查询失败",icon:"fail"})
      // return
    }
    
    // 查询字典码
    const dictRes = await getDict({});
    if (!dictRes && 0 !== dictRes.err_no) {
      console.error(dictRes.err_msg)
      // tt.showToast({title:"字典码获取失败",icon:"fail"})
      // return
    }

    // 查询企业列表
    this.thisGetComInfo()

    // dictRes.data.comArr = comArrOfDict
    tt.setStorageSync("sysConfig", sysRes.data);
    tt.setStorageSync("dict", dictRes.data);
    
    // 顺便给banner赋值
    this.setData({
      bannerList: JSON.parse(sysRes.data.keyAndValue.bannerImgUrl),
    })
    publicFunction.douyinLogin()
  }
  
})