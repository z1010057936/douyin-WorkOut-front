var MD5Util = require('../../utils/md5');

Page({
  data: {
    adminLoginFlag: false,
    account: "Admin",
    buttonList:[
      {
        "tittle":"埋点日志查询",
        "iconSrc":"../../assets/manager/document.png",
        "pageUrl":"/components/managerPage/point-detail/point-detail"
      },
      {
        "tittle":"留资信息管理",
        "iconSrc":"../../assets/manager/document.png",
        "pageUrl":"/components/managerPage/join-detail/join-detail"
      },
      {
        "tittle":"企业信息管理",
        "iconSrc":"../../assets/manager/com.png",
        "pageUrl":"/components/managerPage/com-detail/com-detail"
      },
      {
        "tittle":"职位信息管理",
        "iconSrc":"../../assets/manager/work.png",
        "pageUrl":"/components/managerPage/post-detail/post-detail"
      },
      {
        "tittle":"系统参数管理",
        "iconSrc":"../../assets/manager/setting.png",
        "pageUrl":"/components/managerPage/settings-detail/settings-detail"
      }
    ]
  },
  onLoad: function (options) {

  },

  // 管理员登录验证
  async formSubmit(e) {
    // this.setData({adminLoginFlag:true})

    // console.info("提交",e)
    if("" === e.detail.value.password || "" === e.detail.value.account) {
      tt.showToast({title:"请输入账号密码"})
      return
    }
    const sysConfig = tt.getStorageSync("sysConfig");
    const { adminAandC }  = sysConfig.keyAndValue;
    //加密密码
    var password =  MD5Util.hex_md5(e.detail.value.account+e.detail.value.password);
    console.info(adminAandC,"   ",password)
    if (adminAandC === password) {
      tt.showToast({title:"登录成功"})
      this.setData({adminLoginFlag:true,account:e.detail.value.account})
      tt.setStorageSync("managerInfo", {
        adminAccount: e.detail.value.account,
      });
    }else {
      
      tt.showToast({title:"账号或密码不正确"})
    }
  },

  /**
   * 前往管理页面
   */
   navigateTo(e) {
    console.info("跳转",e.currentTarget.dataset.pageurl)
    const pageurl = e.currentTarget.dataset.pageurl;
      tt.navigateTo({
        url: pageurl,
        success: (res) => {

        },
        fail: (res) => {
          console.log(res);
        },
      });
  },

})