// d:\dev\code\douyin-WorkOut-front\pages\custom-joinLog\custom-joinLog.js
import {
  searchCustomJoinLog
} from "../../api/common";

Page({
  data: {
    distanceSortList:[],
  },
  properties: {

  },
  async onLoad (e) {
    // console.info(e)
    const { cusId } = e
    let data = {
      cusId
    }
    const res = await searchCustomJoinLog(data);
    if (!res) {
      return;
    }
    
    const distanceSortList=res?.data;
    this.setData({
      distanceSortList
    })
  },
  methods: {

  }
})