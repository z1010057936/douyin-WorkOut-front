var publicFunction = require('../../utils/publicFunction');
import {
  getComPostDetail,
  customJoinLogManager
} from "../../api/common";
const app = getApp();
Page({
  
  onReady(e) {
    
  },


  data: {
    postList: null,
    posts: null,
    starChecked: false,
    bindSucess:false,
    isLogin:false,
    current:0,
    ishidden: true,
    fromPage: "index",
    labelList: null,
    nowData: null
  },
  
  async onLoad(options) {
    const { systemInfo } = app.globalData;
    this.setData({
      iPhoneBottomHeight:systemInfo.screenHeight - systemInfo.safeArea.bottom,
    })
    const { postList } = options;
    const { fromPage } = options;
    const thisPostList = JSON.parse(postList)
    const { label } = thisPostList;
    const datas = {"postId":thisPostList.postId}
    const res = await getComPostDetail(datas);
    const isLogin = tt.getStorageSync("isLogin");

    if (!res) {
      return;
    }
    // const swiperImg = res.companies?.map((item) => {
    //   return item.image
    // })
    // console.info(res)
    // //获取公司信息
    // const companiesInfo = this.getProductById(res?.companies, productId);
    //获取工作内容
    const postsContext = res.data.postContextDetail;
    let swiperImg = res.data.postsContextGraphicDetail;
    // 如果介绍图片为空，则塞入一个默认图片
    if(swiperImg.length == 0) {
      swiperImg = [{imageUrl:"../../assets/comBanner/notBanner.png"}]
    }
    // 隐藏骨架屏
    this.removeSkeleton && this.removeSkeleton();
    this.setData({
      // shopList: res.companies?.slice(0,3),
      swiperImg,
      postList: thisPostList,
      posts: postsContext,
      fromPage,
      labelList: label.split(","),
      isLogin
    })
    
  },
  getProductById(comList, id) {
    const companies = comList;
    // 在 products 数组中查找指定 ID 的商品
    const companiesInfo = companies.find(p => p.id === id);
    // 如果找到了商品，返回商品信息；否则返回空对象
    return companiesInfo || {};
  },
  // bind:getgoodsinfo 使用示例
  // 非商品库商品
  getGoodsInfo(event) {
    // const {
    //   goodsId
    // } = event.detail;
    return new Promise(resolve => {
      // 在此处开发者可以进行商品数据请求，获取商品信息
      // 然后将商品信息传入 resolve 函数
      resolve({
        currentPrice: 9900,
        goodsName: '循礼门M+丨【释集烤肉】99元  原价206.4元超值套餐',
        goodsPhoto: 'https://p11.douyinpic.com/img/aweme-poi/product/spu/c050f399ac447daf2715e11e6976c2e2~noop.jpeg?from=3303174740',
        goodsLabels: [{
          type: 'EXPIRED_RETURNS'
        }, // 过期退
        {
          type: 'REFUND_ANYTIME'
        }, // 随时退
        {
          type: 'BOOK_IN_ADVANCE',
          value: 2
        } // 提前2日预约
        ],
        minLimits: 1,
        maxLimits: 2,
        dateRule: '周一至周日可用',
        validation: {
          phoneNumber: {
            required: true // 手机号是否必填, 为 ture则必填，false选填，默认选填
          }
        },
        extra: {}
      });
    });

  },
  // 错误信息含义见下文 bind:error报错信息
  handleError(event) {
    const {
      errMsg,
      errNo
    } = event.detail;
    // console.log(errNo, errMsg);
  },
   userLogin(event) {
    let that = this;
    const { goodsId, goodsType } = event.detail
    return new Promise((resolve, reject) => {
      tt.login({
        success() {
            resolve();
          // 用户登录成功并获取信息，则调用 resolve 函数，跳转至提单页

        },
        fail() {
          // 用户登录失败，则跳转提单页失败
          reject();
        }
      });
    });
  },
  closeDialog(e){
    // console.log(e);
    this.setData({
      showDialog:false
    })
  },

  //立即报名，可触发接口
  joinPost(e) {
    publicFunction.joinPost(e)
  },

  // 触发报名接口
  async customJoinLogManagerInterFace(data) {
      //这里调用后台报名接口
      const res = await customJoinLogManager(data)
      if(0 === res.err_no) {
        tt.showToast({
          title: '报名成功'
        });
      }else{
        tt.showToast({
          title: res.err_msg,
          icon: 'fail'
        });
      }
  },
  


  checkLogin(){
    let  that =this;
    const { isLogin } = that.data;
    if (!isLogin) {
      tt.login({
        success(res) {
          // console.log('??',res);
          // 用户登录成功并获取信息，则调用 resolve 函数，跳转至提单页
          that.setData({
            showDialog:true
          })
        },
        fail() {
          // 用户登录失败，则跳转提单页失败
          console.log("用户登录失败或未授权：",e);
        }
      });
    }
  },

  // // 商品库商品
  // getGoodsInfo(event) {
  //   return new Promise(resolve => {
  //     // 在此处开发者可以进行商品数据请求，获取商品信息
  //     // 然后将商品信息传入 resolve 函数
  //     resolve({
  //       minLimits: 1,
  //       maxLimits: 2,
  //       dateRule: '周一至周日可用',
  //       validation: {
  //         phoneNumber: {
  //           required: true // 手机号是否必填
  //         }
  //       }
  //     });
  //   })
  // }
  onTabCollect() {
    const { starChecked } = this.data;
    this.setData({
      starChecked: !starChecked,
    })
  },
   bindPhone(){
       return false;
  },
  onTapGetPhone(e){
    //  console.log('aaa',e);
    //  this.setData({
    //    showDialog:false
    //  })
    this.setData({
      bindSucess:true,
      isLogin:true,
    })
  },
  switchTap(e){
    const {current} = e.detail;
    this.setData({
      current,
    })
  },
  success(e){
    // console.log(e.detail);
    // 开发者可在此处进行手机号解密以及绑定工作
    this.setData({
      showDialog:false,
    })
  },

  //跳转到导航页，传入企业经纬度
  gotoAddress() {
    this.data.ishidden = false;
    const { postList } = this.data;
    if ((null !== postList.longitude && '' !== postList.longitude) &&
    (null !== postList.latitude && '' !== postList.latitude)) {
      tt.navigateTo({
        url: `/pages/mapArea/open-map-app?destination=${postList.fullAddress}&longitude=${postList.longitude}&latitude=${postList.latitude}`,
        success: (res) => {
          
        },
        fail: (res) => {
          console.log(res);
        },
      });
    } else {
      tt.showToast({
        title: "暂无位置信息",
        icon:"none"
      });
    }
      
  },

  //调用用户拨打电话功能
  tapMakePhoneCall(e) {
    publicFunction.tapMakePhoneCall(e);
  },

  /**
     * 一键登录
     * @param {*} e 
     */
   async douyinLogin(e) {
    console.log("---- 点击登录", this.data.openCounts)
    this.setData({
      loginLoadding: true
    })
    const res = await publicFunction.douyingetPhoneNum(e)
    if(null !== res) {
      await publicFunction.resumeInit(res, this)
      tt.showTabBar({animation: false})
      await publicFunction.joinPost(this.data.nowData)
      this.setData({
        loginLoadding: false
      })
    }else {
      tt.showTabBar({animation: false})
      this.setData({
        loginLoadding: false
      })
    }
  },

  /**
     * 一个用于阻止父级组件事件冒泡的空方法，顺便塞个当前点击的列数据
     */
   noFunction(e) {
    // console.info("----noFunction",e)
    this.setData({
      nowData: e
    })
  },

})