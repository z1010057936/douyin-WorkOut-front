import {
  getCustomResume,
  customResumeManage
} from "../../api/common";
var validate = require("../../utils/validata");
var publicFunction = require('../../utils/publicFunction');

Page({
  data: {
    bodyStatusIndex: 0,
    isShowTips: true,
    bodyStatusArray: [],
    salaryUnitArray: ["/月", "/日"],
    salaryUnitIndex: 0,
    sexArray: [{
      "key": "1",
      "value": "男"
    }, {
      "key": "2",
      "value": "女"
    }],
    tempResData: {},
    formData: {},
    optFlag: 0,
    // 设置字段校验规则
    initValidate: {
      name: {
        required: '请填写姓名',
        cc: '请输入正确的姓名'
      },
      sex: {
        required: '请选择性别',
      },
      // 2023年10月27日 根据帅新卫需求简化简历填写页
      // bodyStatus: {
      //   required: '请选择健康状况',
      // },
      // idno: {
      //   required: '请输入身份证号',
      //   idcard: '请输入正确的身份证号'
      // },
      phoneNum: {
        required: '请输入手机号',
        tel: '请输入正确的手机号'
      },
      wantPost: {
        // required: '请输入期望职位',
        cc: '请输入正确的期望职位'
      },
      wantCity: {
        // required: '请输入期望城市',
        cc: '请输入正确的期望城市'
      },
      // wantSalary: {
      //   required: '请输入期望薪资',
      //   // salary: '请输入正确的期望薪资'
      // },
      // 2023年10月10日 帅新卫提出需求，删除紧急联系人相关信息的录入
      // emContactName: {
      //   required: '请输入紧急联系人姓名',
      //   cc: '请输入正确的紧急联系人姓名'
      // },
      // emContactPhoneNum: {
      //   required: '请输入紧急联系手机号',
      //   tel: '请输入正确的紧急联系手机号'
      // },
  },
  },

  async onLoad(e) {
    // console.info(e)
    let customBaseInfo = JSON.parse(tt.getStorageSync("customBaseInfo"));
    const dictList = tt.getStorageSync("dict");
    const { bodyStatus } = dictList.publicDict;

    // 为健康状况下拉框转换字典数据
    let bodyStatusArr = [];
    let whileFlag = true;
    let index = 0;
    while(whileFlag) {
      if(undefined !== bodyStatus[index]) {
        bodyStatusArr[index] = bodyStatus[index]
        index ++;
      }else{
        whileFlag = false
      }
    }


    var {
      cusId
    } = customBaseInfo;
    tt.showLoading({
      title: "请稍后...",
      mask: true
    })
    const customResumeRes = await getCustomResume({"cusId": cusId});
    // 如果查询数据不为空，则将其赋值给customResumeRes用于后续业务及展示，否则只塞入cusId
    if (0 === customResumeRes.err_no) {
      if (null === customResumeRes.data) {
        customBaseInfo.cusId = cusId
      } else {
        //处理一下期望薪资的格式 xxx-xxx/月
        customBaseInfo = customResumeRes.data
        let { wantSalary } = customBaseInfo
        if(!publicFunction.isEmpty(wantSalary)) {
          // console.info("wantSalary",wantSalary)
        // 检查用户输入的符号是什么
        let wantSalary2CharOfE = wantSalary.indexOf("-")
        let wantSalary2CharOfC = wantSalary.indexOf("—")
        //检查符号位置
        let wantSalary2Char = wantSalary2CharOfE == -1?wantSalary2CharOfC:wantSalary2CharOfE;
        // 检查单位位置
        let wantSalary4Char = wantSalary.indexOf("/")

        // 分割字符串
        let wantSalary1 = wantSalary.substr(0,wantSalary2Char);
        // let wantSalary2 = wantSalary.substr(wantSalary2Char,wantSalary4Char)
        let wantSalary3 = wantSalary.substr(wantSalary2Char+1,wantSalary4Char-wantSalary2Char-1)
        let wantSalary4 = wantSalary.substr(wantSalary4Char,wantSalary.length)
        // console.info(wantSalary1,wantSalary3,wantSalary4)

        customBaseInfo.wantSalary1 = wantSalary1;
        customBaseInfo.wantSalary3 = wantSalary3;
        customBaseInfo.wantSalary4 = "/月"==wantSalary4?0:1;
        }
        
      }
    } else {
      tt.showToast({
        title: res.err_msg,
        icon: 'fail'
      });
    }

    this.setData({
      tempResData: customBaseInfo,
      bodyStatusIndex: customBaseInfo.bodyStatus,
      bodyStatusArray: bodyStatusArr,
      salaryUnitIndex: customBaseInfo.wantSalary4
    })
    tt.hideLoading({});

  },

  // ruleCheck(e) {
  //   const value = e.detail.value; //输入的值
  //   const rulerName = e.target.dataset.name; // 该输入框的key
  //   const rulers = this.data.ruler[rulerName]; // 校验规则
  //   const patterns = new RegExp(rulers.pattern)


  // },

  //健康状况选项框变更触发
  bodyStatusChange(e) {
    this.setData({
      bodyStatusIndex: e.detail.value
    });
  },
  
  salaryUnitChange(e) {
    this.setData({
      salaryUnitIndex: e.detail.value
    });
  },

  // 表单提交
  formSubmit(e) {
    let dataInfo = e.detail.value;
    // 如果期望薪资不为空，再进行以下校验
    if("" !== dataInfo.wantSalary1 || "" !== dataInfo.wantSalary3) {
      if (Number(dataInfo.wantSalary1) > Number(dataInfo.wantSalary3)) {
        tt.showModal({
          title: "期望薪资格式不正确",
          content:"期望薪资起始值不能大于结束值",
          showCancel:false
        });
        return
      }
      dataInfo.wantSalary = dataInfo.wantSalary1 + "-" + dataInfo.wantSalary3 + this.data.salaryUnitArray[dataInfo.wantSalary4]
    }
    

    if(validate.initFormRule(dataInfo, this.data.initValidate) === false) {
      return
    }

    //初始化数据
    dataInfo.cusId = this.data.tempResData.cusId
    dataInfo.optFlag = this.data.optFlag;
    let params = dataInfo

    const isLogin = tt.getStorageSync("isLogin");
    var message = "您确定提交吗？";
    if (!isLogin && !this.data.tempResData.phone) {
      message = "未登录，终止操作";
      return
    }
    let self = this;



    tt.showModal({
      content: message,
      confirmText: "确定",
      cancelText: "关闭",
      success(res) {
        if (res.confirm) {
          tt.showLoading({
            mask: true,
            title: "请稍后..."
          });
          self.customResumeManageInterface(params)
          var hideTime = setTimeout(() => {
            tt.hideLoading({});
          }, 10000);
          clearTimeout(hideTime);
        }
      },
    });

  },

  //调用简历信息管理接口
  async customResumeManageInterface(datas) {
    datas.cusId = this.data.tempResData.cusId;
    const res = await customResumeManage(datas);
    if (0 === res.err_no) {
      tt.setStorageSync("customBaseInfo", JSON.stringify(datas));
      var navigateBackTime = setTimeout(() => {
        tt.navigateBack({
          delta: 1
        })
      }, 1000);
      clearTimeout(navigateBackTime);
      tt.hideLoading({});
      tt.showToast({
        title: '提交成功',
        icon: 'success'
      });
    } else {
      tt.showToast({
        title: res.err_msg,
        icon: 'fail'
      });
    }
  },

  //表单校验
  formRule(e) {
    var result = true;
    const keys = Object.keys(e);
    keys.forEach(function (item, key) {
      if (e[item] === "" || e[item] === null || e[item].length === 0) {
        tt.showToast({
          title: "请完整填写数据",
          icon: 'fail'
        });
        result = false;
      }
    })
    return result;
  },

  // 重置
  // formReset(e) {
  //   this.setData({
  //     tempResData: this.data.tempResData,
  //     bodyStatusIndex: this.data.tempResData.bodyStatus
  //   });
  // }


});