var publicFunction = require('../../utils/publicFunction');

import {
  getComPostInfo
} from "../../api/common";
import { config } from  "../../api/config"
const app = getApp();
Page({
  
  onShow(e) {
    const isLogin = tt.getStorageSync("isLogin");
    this.setData({
      isLogin
    })
  },

  data: {
    comList: null,
    posts: null,
    starChecked: false,
    bindSucess:false,
    isLogin:false,
    current:0,
    ishidden: true,
    fromPage: "index",
    labelList: null,
    fileBasePath: config.fileBasePath,
    swiperImg: []
  },
  
  async onLoad(options) {
    const { systemInfo } = app.globalData;
    this.setData({
      iPhoneBottomHeight:systemInfo.screenHeight - systemInfo.safeArea.bottom,
    })

    // let fileBasePath = config.fileBasePath
    // this.setData({
    //   fileBasePath
    // })
    
    const { comList } = options;
    // json化企业基础信息
    const thiscomList = JSON.parse(comList)
    // const swiperImg = res.companies?.map((item) => {
    //   return item.image
    // })
    
    // 获取
    let swiperImg = thiscomList.bannerImgUrl;
    // 如果介绍图片为空，则塞入一个默认图片
    if(undefined == swiperImg || swiperImg.length == 0) {
      swiperImg = []
    }
    this.setData({
      // shopList: res.companies?.slice(0,3),
      swiperImg,
      comList: thiscomList,
    })

    let data = {"comId":thiscomList.comId, page: 1, limit: 50}
    const res = await getComPostInfo(data);
    // console.info("res:",res)
    if (!res) {
      return;
    }
   const distanceSortList=res.data.records;
   tt.hideLoading({});
   // 隐藏骨架屏
   this.removeSkeleton && this.removeSkeleton();
    this.setData({
      distanceSortList
    })
    
  },

  //立即报名，可触发接口
  joinPost(e) {
    publicFunction.joinPost(e)
  },

  //跳转到导航页，传入企业经纬度
  gotoAddress() {
    this.data.ishidden = false;
    const { comList } = this.data;
    if ((null !== comList.longitude && '' !== comList.longitude) &&
    (null !== comList.latitude && '' !== comList.latitude)) {
      tt.navigateTo({
        url: `/pages/mapArea/open-map-app?destination=${comList.fullAddress}&longitude=${comList.longitude}&latitude=${comList.latitude}`,
        success: (res) => {
          
        },
        fail: (res) => {
          console.log(res);
        },
      });
    } else {
      tt.showToast({
        title: "暂无位置信息",
        icon:"none"
      });
    }
      
  },

  switchTap(e){
    const {current} = e.detail;
    this.setData({
      current,
    })
  },

  /**
   * 由login-dialog传递过来的登录结果
   * @param {*} e 
   */
  getLoginResult(e) {
    console.info("----getLoginResult", e.detail)
    const isLogin = e.detail.isLogin
    this.setData({
      isLogin
    })
  }


})